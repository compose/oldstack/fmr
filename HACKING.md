# Notes for developers

## Continuous integration

CI jobs are defined in *.gitlab-ci.yml*

The gitlab runners come from the "diodon" project on the [Inria ci portal](https://ci.inria.fr/project/diodon/slaves).

Two slaves have been defined.

One virtual machine Ubuntu with docker installed and the other one with guix installed.

The "docker" job allows to build a docker image with current state of FMR installed in the system with Chameleon MPI enabled.

The "sonarqube" job performs a static/dynamic analysis of the source code and sends reports to [sonarqube](https://sonarqube.inria.fr/sonarqube/dashboard?id=compose%3Alegacystack%3Afmr).
The job is executed inside an existing [docker image](https://gitlab.inria.fr/solverstack/docker) where analysis tools are already installed as well as chameleon.
This image is private so that we use the [DOCKER_AUTH_CONFIG](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#define-an-image-from-a-private-container-registry) to authenticate (defined as an environment variable during the CI job).

The other jobs perform ctest checking inside a guix environment for different configurations: lapack-mkl, lapack-openblas, chameleon-mkl, chameleon-openblas.

### Hints about the Linux virtual machine installation with docker

```
sudo apt update
sudo apt upgrade

sudo apt install curl git vim

# docker installation
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker ${USER}
newgrp docker

# gitlab-runner installation
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
sudo chmod +x /usr/local/bin/gitlab-runner
sudo gitlab-runner install --user=ci --working-directory=/builds
sudo gitlab-runner start

sudo gitlab-runner register
# follow instructions for your project
# use docker executor

sudo gitlab-runner stop
# change the volumes parameter of your docker runner
sudo vim /etc/gitlab-runner/config.toml
# volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
sudo gitlab-runner start

sudo gitlab-runner verify
```

### Hints about the Mac OS X virtual machine installation

We have used the virtual machine available at https://ci.inria.fr/ under the
name 'CI-MacOSX-Catalina-10.15.4-xcode' with the 'xxLarge' configuration meaning
4 CPUs at 2Ghz and 12GB of RAM. On this machine git, gcc and cmake are already
installed. We have installed the other required components as follows:
```
brew install cmake gcc hdf5-mpi libomp openblas pkg-config
brew install -v --cc=clang --build-from-source ~/brew-repo/starpu.rb
brew install -v --cc=clang --build-from-source ~/brew-repo/chameleon.rb

# use pkg-config .pc files to detect some dependencies
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/local/opt/openblas/lib/pkgconfig
# cmake checks blas.pc not openblas.pc
sudo cp /usr/local/Cellar/openblas/0.3.26/lib/pkgconfig/openblas.pc /usr/local/Cellar/openblas/0.3.26/lib/pkgconfig/blas.pc
```
