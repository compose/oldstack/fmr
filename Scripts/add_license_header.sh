#!/bin/bash
LICENSE_FILE=$1
files=`find . -name "*.hpp" -o -name "*.cpp" | xargs`
for i in $files
do
    echo $i
    dos2unix $i
    cat $LICENSE_FILE $i > filetmp
    mv filetmp $i
done
git checkout Packages/compress_stream Packages/half
cd modules/internals/cpp_tools && git checkout . && cd -
