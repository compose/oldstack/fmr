#!/bin/bash
set -ex

# ensure starpu and chameleon are installed
for dep in starpu chameleon
do
  DEP_INSTALLED=`brew ls --versions ${dep} | cut -d " " -f 2`
  if [[ -z "${DEP_INSTALLED}" ]]; then
    # dep is not installed, we have to install it
    brew install --build-from-source ./Scripts/ci/${dep}.rb
  else
    # dep is already installed, check the version with our requirement
    DEP_REQUIRED=`brew info --json ./Scripts/ci/${dep}.rb |jq -r '.[0].versions.stable'`
    if [[ "${DEP_INSTALLED}" != "${DEP_REQUIRED}" ]]; then
      # if the installed version is not the required one, re-install
      brew remove --force --ignore-dependencies ${dep}
      brew install --build-from-source ./Scripts/ci/${dep}.rb
    fi
  fi
done

export CC=/usr/local/bin/gcc-14
export CXX=/usr/local/bin/g++-14
export FC=/usr/local/bin/gfortran-14
[[ -d build ]] && rm -rf build
cmake -B build -DFMR_BUILD_TESTS=ON -DFMR_USE_HDF5=ON -DFMR_USE_CHAMELEON=ON \
               -DFMR_USE_MPI=OFF \
               -DBLA_PREFER_PKGCONFIG=ON -DCMAKE_INSTALL_PREFIX=$PWD/install \
               -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="-Wfatal-errors"
cmake --build build > /dev/null
ctest --test-dir build --output-on-failure --no-compress-output --output-junit ../junit.xml
