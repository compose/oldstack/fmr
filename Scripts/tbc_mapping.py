from math import *
from itertools import product
import random
import itertools

# requires c prime
# requires N is a multiple of c*c
# uses c*(c+1) procs

## result: P = c*(c+1) processors
##       list of length P, each contains the tiles that P control
##       plus list of length N = c*c, each contains the list of processors that know this row
def pattern_tbc(c, diag_first=False):
    N = c*c

    def tbc_index(i, j, u):
        if u == 0: return j
        return u*c + ((i + j*(u-1)) % c)
    def triangle(row_indices):
        tiles = []
        for y in R:
            for x in R:
                if x < y:
                    tiles.append((x, y))
        return tiles

    row_assignments = [ [] for _ in range(c*c) ]

    result = []
    if diag_first:
        # Fill diagonal procs
        for i in range(c):
            R = list(range(c*i, c*(i+1)))
            for r in R:
                # current proc is len(result)
                row_assignments[r].append(len(result))
            result.append(triangle(R))

    # Fill procs with TBC
    for j in range(c):
        for i in range(c):
            R = [ tbc_index(i, j, u) for u in range(c) ]
            for r in R:
                # current proc is len(result)
                row_assignments[r].append(len(result))
            result.append(triangle(R))

    if not diag_first:
        # Fill diagonal procs
        for i in range(c):
            R = list(range(c*i, c*(i+1)))
            for r in R:
                # current proc is len(result)
                row_assignments[r].append(len(result))
            result.append(triangle(R))

    return result, row_assignments




def make_grid(pattern, pattern_size):
    grid = [ [None] * pattern_size for i in range(pattern_size) ]
    for (i, tiles) in enumerate(pattern):
        for x, y in tiles:
            assert grid[x][y] is None, f"g[{x}][{y}] should be None, not {grid[x][y]}"
            grid[x][y] =  i
            if x != y:
                grid[y][x] = i
    return grid

def symmetrize(pattern):
    return pattern + [ (y, x) for (x, y) in pattern if x != y]

def offset(coord, pattern):
    a, b = coord
    for x, y in pattern:
        yield (x+a, y+b)

def cyclicify(pattern, row_assignments, nb_of_patterns, additional_lines=0, skip_greedy=False, verb=False, is_symmetric=True):
    pattern_size = len(row_assignments)
    if is_symmetric:
        sym_pattern = [ symmetrize(l) for l in pattern ]
    else:
        sym_pattern = pattern
        pattern = [ [x for x in tiles if x[0] > x[1]] for tiles in pattern ]
    P = len(pattern)
    result = [[] for _ in range(P)]

    def apply_pattern(i, j, pattern_to_apply, row=None, max_column=None):
        coord = (i*pattern_size, j*pattern_size)
        for p in range(P):
            if row is not None:
                selected, replacement = row
                limited_pattern = [(x, replacement) for x, y in pattern_to_apply[p]
                                   if y == selected and (max_column is None or x < max_column)]
            else:
                limited_pattern = pattern_to_apply[p]
            result[p].extend(offset(coord, limited_pattern))

    for i in range(nb_of_patterns):
        for j in range(i):
            apply_pattern(j, i, sym_pattern)
        apply_pattern(i, i, pattern)

    # Assign additional lines, trying to keep the pattern as balanced as possible
    if additional_lines:
        selected_rows = []
        def load_of_row(pattern_of_proc, row):
            return sum(1 for (x, y) in pattern_of_proc if y == row)
        load_of_each_row = [ [load_of_row(u, row) for u in sym_pattern] for row in range(pattern_size) ]

        def get_score(row):
            scores_per_proc = [ (load_of_each_proc[p] + (0.5+nb_of_patterns)*load_of_each_row[row][p])**2 for p in range(P) ]
            return max(scores_per_proc), -min(scores_per_proc), sum(s**5 for s in scores_per_proc)

        pattern_grid = make_grid(pattern, pattern_size)
        # Choose the selected rows with local search. Start with a random selection
        selected = set(random.sample(list(range(pattern_size)), k=additional_lines))
        non_selected = set(range(pattern_size))
        non_selected.difference_update(selected)
        # Load: each selected row counts nb_of_patterns times
        load_of_each_proc = [0] * P
        for row in selected:
            for p in range(P):
                load_of_each_proc[p] += nb_of_patterns * load_of_each_row[row][p]
        # Plus the last diagonal block
        for (i, j) in itertools.combinations(selected, 2):
            load_of_each_proc[pattern_grid[i][j]] += 1

        def get_score(loads):
            return max(loads), -min(loads), sum(x ** 5 for x in loads)

        def compute_new_score(to_remove, to_add):
            new_loads = load_of_each_proc.copy()
            for p in range(P):
                new_loads[p] -= nb_of_patterns * load_of_each_row[to_remove][p]
                new_loads[p] += nb_of_patterns * load_of_each_row[to_add][p]
            for r in selected:
                if r != to_remove:
                    new_loads[pattern_grid[to_remove][r]] -= 1
                    new_loads[pattern_grid[to_add][r]] += 1

            return new_loads, get_score(new_loads)

        stop = False
        while not stop:
            highest_load, lowest_load = max(load_of_each_proc), min(load_of_each_proc)
            original_score = get_score(load_of_each_proc)
            most_loaded = [ p for p in range(P) if load_of_each_proc[p] == highest_load ]
            least_loaded = [ p for p in range(P) if load_of_each_proc[p] == lowest_load ]
            def get_nb_candidates(candidates, row):
                return sum(1 for p in candidates if load_of_each_row[row][p] != 0)
            to_remove = max(selected, key=lambda r: get_nb_candidates(most_loaded, r))
            to_add = max(non_selected, key=lambda r: get_nb_candidates(least_loaded, r))

            best_score = original_score
            best_loads = None
            best_candidates = None
            for r, a in itertools.product(selected, non_selected):
                tentative_loads, tentative_score = compute_new_score(r, a)
                if tentative_score < original_score:
                    best_score = tentative_score
                    best_loads = tentative_loads
                    best_candidates = r, a

            if best_candidates is not None:
                to_remove, to_add = best_candidates
                load_of_each_proc = best_loads
                selected.remove(to_remove)
                selected.add(to_add)
                non_selected.remove(to_add)
                non_selected.add(to_remove)
            else:
                stop = True

        selected_rows = list(selected)
        for i, selected_row in enumerate(selected_rows):
            for j in range(nb_of_patterns):
                apply_pattern(j, nb_of_patterns, sym_pattern, row=(selected_row, i))
        sub_pattern = [ [] for p in range(P) ]
        for i, ri in enumerate(selected_rows):
            for j, rj in enumerate(selected_rows):
                if i < j:
                    proc = pattern_grid[ri][rj]
                    sub_pattern[proc].append((i, j))
        apply_pattern(nb_of_patterns, nb_of_patterns, sub_pattern)


    if skip_greedy:
        return result

    if verb:
        print("Before greedy")
        lens= [len(u) for u in result]
        print(lens)
        check(result, nb_of_patterns*pattern_size + additional_lines)

    def assign_tile(candidates, x, y):
        chosen = min(candidates, key=lambda p: len(result[p]))
        result[chosen].append((x, y))

    def assign_regular(j, i):
        coord = (j*pattern_size, i*pattern_size)
        for k in range(pattern_size):
            assign_tile(row_assignments[k], coord[0] + k, coord[1] + k)

    # Finally: distribute diagonal tiles
    for i in range(nb_of_patterns):
        for j in range(i):
            assign_regular(j, i)

    # Special case for the additional lines, where the empty tiles are not exactly placed at diagonal places
    if additional_lines:
        for k, row in enumerate(selected_rows):
            candidates = row_assignments[row]
            for j in range(nb_of_patterns):
                coord_x, coord_y = (j*pattern_size + row, nb_of_patterns*pattern_size + k)
                if coord_y > coord_x:
                    assign_tile(candidates, coord_x, coord_y)
            assign_tile(candidates, nb_of_patterns*pattern_size + k, nb_of_patterns*pattern_size + k)

    # Assign 'true' diagonal tiles of the matrix at the end
    # Goal: as much variety as possible on the diagonal
    coords = [ (k, i) for k in range(pattern_size) for i in range(nb_of_patterns) ]
    random.shuffle(coords)
    for k, i in coords:
        assign_tile(row_assignments[k], i*pattern_size + k, i*pattern_size + k)

    return result

def nb_comms(pattern):
    def count_for_one_proc(assignment):
        rows = set()
        for v in assignment:
            rows.update(v)
        return len(rows)
    return [ count_for_one_proc(a) for a in pattern ]

def nb_procs_in_colrows(grid):
    def count_for_one_colrow(i):
        procs = set(grid[i]) ## procs on the row
        procs.update(grid[j][i] for j in range(len(grid))) ## procs on the column
        return len(procs)
    return [count_for_one_colrow(i) for i in range(len(grid))]

# requires c prime
# requires N is a multiple of c*c
# uses c*(c+1) procs
def partbc(c, N, diag_first=False, **args):
    p = c*(c+1)
    b = (N // (c*c))
    additional_lines = N % (c*c)

    pattern, row_assignments = pattern_tbc(c, diag_first)
    return cyclicify(pattern, row_assignments, b, additional_lines=additional_lines, **args)

def output_pattern_R(pattern, out):
    for (i, tiles) in enumerate(pattern):
        for t in tiles:
            print(t[0], t[1], i, file=out)

def output_grid(grid, out):
    print(len(grid), len(grid[0]), file=out)
    for line in grid:
        print(*line, file=out)

def check(pattern, n, grid=None):
    lens= [len(u) for u in pattern]
    print("Final result:")
    print("Avg load:", sum(lens)/len(lens))
    print("Load balance:", lens)
    print("Gap:", min(lens), max(lens), (max(lens)-min(lens))/(sum(lens)/len(lens)))
    print("Overhead:", (max(lens))/(sum(lens)/len(lens)) - 1)
    print("Nb rows accessed by each proc:", nb_comms(pattern))

    print("Check that all tiles are assigned:", sum(lens), n*(n+1)//2)
    if grid is not None:
        print("Nb procs in each colrow:", nb_procs_in_colrows(grid))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("TBC-based pattern")

    parser.add_argument("-c", type=int, default=5, help="Value of c. Must be prime")
    parser.add_argument("-n", type=int, default=None, help="Value of n. Must be a multiple of c*c. Defaults to c*c")
    parser.add_argument("-o", type=argparse.FileType("w"), default = None, help="File for output")
    parser.add_argument("--skip-greedy", dest="skipgreedy", action="store_true", help="Skip the Greedy step")
    parser.add_argument("--r-format", dest="r", action="store_true", help="Use R format for output")
    parser.add_argument("-v", help="verbose", action="store_true")
    parser.add_argument("--plot", action="store_true", help="Plot the pattern as a PDF file (requires plotnine)")
    parser.add_argument("--box", action="store_true", help="Draw boxes around the patterns")
    parser.add_argument("--largebox", action="store_true", help="Draw a box around the whole matrix")
    parser.add_argument("--squares", action="store_true", help="Draw lines around the tiles")
    parser.add_argument("--highlight", default=None, type=int, help="Highlight a particular row in the pattern")
    parser.add_argument("--colors", nargs="+", help="list of colors in the palette. If present, need c(c+1) colors")
    parser.add_argument("--label", action="store_true", help="Include label of nodes")
    parser.add_argument("--diag-first", action="store_true", help="Number nodes on the diagonal zones first")

    args = parser.parse_args()
    if not args.n:
        args.n = args.c*args.c
    try:
        pattern = partbc(args.c, args.n, skip_greedy=args.skipgreedy, verb=args.v, diag_first=args.diag_first)
    except AssertionError:
        print("N needs to be a multiple of c*c")
        exit(1)
    try:
        grid = make_grid(pattern, args.n)
    except AssertionError:
        print("Colliding allocation. Are you sure that c is prime?")
        exit(1)
    if args.v:
        check(pattern, args.n, grid)

    print("Resulting pattern has", args.c*(args.c+1), "processors")
    if args.o:
        if args.plot:
            import pandas
            from plotnine import *

            values = [ (t[0], t[1], i) for (i, tiles) in enumerate(pattern) for t in tiles]
            xs, ys, indices = zip(*values)
            xmax = args.n-1

            df = pandas.DataFrame({"x": xs, "y": ys, "node": indices})
            df['y'] = xmax - df['y']
            p = ggplot(df, aes(x="x", y="y", height=1, width=1, fill="factor(node+1)"))
            if args.squares:
                p = p + geom_tile(color="black")
            else:
                p = p + geom_tile()
            segment_size = min(2, 1.5/(args.n/27))
            def my_segment(x1, x2, y1, y2, **args):
                return geom_segment(aes(x=x1-0.5, xend=x2-0.5, y=y1-0.5, yend=y2-0.5), size=segment_size, **args)
            if args.box:
                step = args.c*args.c
                for i in range(0, args.n, step):
                    p = p + my_segment(0, i + step, args.n - i, args.n - i)
                    p = p + my_segment(i+step, i+step, args.n-i, 0)
            if args.largebox or args.box:
                p = p + my_segment(0, 0, 0, args.n)
                p = p + my_segment(0, args.n, 0, 0)
                pass
            if args.largebox:
                p = p + my_segment(0, args.n, args.n, args.n)
                p = p + my_segment(args.n, args.n, 0, args.n)
            if args.highlight:
                h = args.highlight
                def add_geom_path(p, l, **args):
                    src = None
                    for dst in l:
                        if src is not None:
                            p = p + my_segment(src[0], dst[0], src[1], dst[1], **args)
                        src=dst
                    return p

                p = add_geom_path(p, [(-0.4, h-1-0.2), (args.n-h-0.2, h-1-0.2), (args.n-h-0.2, -0.4),
                                      (args.n-h+1+0.3, -0.4), (args.n-h+1+0.3, h+0.3), (-0.4, h+0.3), (-0.4, h-1-0.2)],
                                  color="#00CC00")
            if args.label:
                p = p + geom_label(aes(label="node+1"))
            p = p + theme_void() + theme(legend_position="none")
            p = p + expand_limits(x=xmax+1, y=xmax+1)
            if args.colors:
                print(args.colors)
                def add_prefix_if_absent(val, prefix='#'):
                    if val[0] != prefix:
                        return prefix + val
                    return val
                args.colors = [ add_prefix_if_absent(v) for v in args.colors ]
                assert len(args.colors) == args.c*(args.c+1), "Wrong number of colors: should be %d, got %d" % (args.c*(args.c+1), len(args.colors))
                print(args.colors)
                p = p + scale_fill_manual(args.colors)
            p.save(args.o.name, format="pdf", width=5, height=5)
        elif args.r:
            output_pattern_R(pattern, args.o)
        else:
            output_grid(grid, args.o)

