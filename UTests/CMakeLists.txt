﻿set(tests
    testDenseWrappers
    testGemm
    testQRF
    testLapackSVD
    testSVD
    testRSVD
    testREVD
	)

if(FMR_USE_HDF5)
  list(APPEND tests testReadH5)
endif()

foreach(_test ${tests})
  add_executable(${_test} ${_test}.cpp)
  target_link_libraries(${_test} PRIVATE fmr)
endforeach()

if(FMR_USE_HDF5)
  add_custom_command(TARGET testReadH5 POST_BUILD
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${${PROJECT_NAME}_SOURCE_DIR}/Data/hdf5/ ${CMAKE_CURRENT_BINARY_DIR})
endif()

#-------- Tests ---------
include(CTestLists.cmake)
