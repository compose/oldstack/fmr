/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <iomanip>
#include <iostream>
#include <limits>
#include <random>

#include <cpp_tools/colors/colorized.hpp>
#include <cpp_tools/cl_parser/help_descriptor.hpp>
#include <cpp_tools/cl_parser/tcli.hpp>
#include "commonParameters.hpp"
#include "fmr/Utils/Display.hpp" // To display the matrices in verbose mode
#include "fmr/Utils/MatrixNorms.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#if defined(FMR_USE_MPI)
#include "mpi.h"
#endif
#endif
#include "fmr/StandardLRA/Gemm.hpp"

using namespace fmr;

static int nbTest = 0;
static int nbPassed = 0;

#define NEWTEST ++nbTest
#define TESTRESULT(RES)                                                        \
  {                                                                            \
    if (RES) {                                                                 \
      std::cout << "\033[32m[Test " << nbTest << "] PASSED\033[0m"             \
                << std::endl;                                                  \
      ++nbPassed;                                                              \
    } else {                                                                   \
      std::cout << "\033[31m[Test " << nbTest << "] FAILED\033[0m"             \
                << std::endl;                                                  \
    }                                                                          \
  }

#define TESTSUMARY                                                             \
  if (nbTest == 0) {                                                           \
    std::cout << "No tests to summarise" << std::endl;                         \
    return 0;                                                                  \
  } else if (nbPassed == nbTest) {                                             \
    std::cout << "\033[32m";                                                   \
  } else if (nbPassed == nbTest - 1) {                                         \
    std::cout << "\033[33m";                                                   \
  } else {                                                                     \
    std::cout << "\033[31m";                                                   \
  }                                                                            \
  std::cout << std::setprecision(0) << std::fixed;                             \
  std::cout << "Passed " << nbPassed << "/" << nbTest << " - ["                \
            << 100.0 * nbPassed / nbTest << "%]\033[0m" << std::endl;

#define TESTRETURN (nbTest == nbPassed) ? EXIT_SUCCESS : EXIT_FAILURE

template <typename FReal> bool testBlas(FReal prec, bool verbose = false);
template <typename Size, typename Real>
Real cmpBlasCham(const Size nbRowsA, const Size nbColsA, const Size nbColsB,
                 int mpiRank);

int main(int argc, char **argv) {
  using FReal = float;
  using FSize = std::int64_t;
  int mpiRank = 0;

  /* Parameter handling */
  auto parser = cpp_tools::cl_parser::make_parser(
      cpp_tools::cl_parser::help{}, fmr::parameters::nbrows{},
      fmr::parameters::nbcols{}, fmr::parameters::nbcolsb{},
      fmr::parameters::epsilon{},fmr::parameters::nbthreads{},
      fmr::parameters::verbose{}
      );
  parser.parse(argc, argv);

  const FSize NbRowsA = parser.get<fmr::parameters::nbrows>();
  const FSize NbColsA = parser.get<fmr::parameters::nbcols>();
  const FSize NbColsB = parser.get<fmr::parameters::nbcolsb>();
  const FReal precision = parser.get<fmr::parameters::epsilon>();
  const int nbThreads = parser.get<fmr::parameters::nbthreads>();
  const bool verbose = parser.get<fmr::parameters::verbose>();

  if (verbose) {
      std::cout << "This test is in two parts :" << std::endl;
      std::cout << "1/ It tests the BlasDenseWrapper Gemm with fixed matrices" << std::endl;
      std::cout << "2/ It tests the ChameleonDenseWrapper Gemm with random matrix" << std::endl;
  }

  std::cout << "testGEMM: NbRowsA " << NbRowsA << std::endl;
  std::cout << "          NbColsA " << NbColsA << std::endl;
  std::cout << "          NbColsB " << NbColsB << std::endl;
  std::cout << "          NbThreads " << nbThreads << std::endl;

#if defined(FMR_CHAMELEON)
  Chameleon::Chameleon chameleon(2, 0, 320);
  mpiRank = chameleon.getMpiRank();
#endif
  // Call Tests
  if (mpiRank == 0) {
    NEWTEST;
    if (verbose) {
      std::cout << "----------------------------------" << std::endl;
      std::cout << "[FMR] TestGemm Parameters Summary" << std::endl;
      std::cout << "[FMR] NbThreads\t" << nbThreads << std::endl;
      std::cout << "[FMR] Precision\t" << precision << std::endl;
      std::cout << "----------------------------------" << std::endl;
      std::cout << "Starting test " << nbTest << ": testing BlasDenseWrapper"
                << std::endl;
    }
    TESTRESULT(testBlas<FReal>(precision, verbose));
  }
#if defined(FMR_CHAMELEON)
  NEWTEST;
  if (verbose && mpiRank == 0) {
    std::cout << "Starting test " << nbTest << ": testing ChameleonDenseWrapper"
              << std::endl;
    std::cout << "----------------------------------" << std::endl;
    std::cout << "[FMR] A is " << NbRowsA << " by " << NbColsA << std::endl;
    std::cout << "[FMR] B is " << NbColsA << " by " << NbColsB << std::endl;
    std::cout << "----------------------------------" << std::endl;
  }
  FReal Vartest = cmpBlasCham<FSize, FReal>(NbRowsA, NbColsA, NbColsB, mpiRank);
  if (mpiRank == 0) {
    if (verbose) {
      std::cout << "Result of test " << nbTest
                << ": ||Ccham - Cblas|| / ||Cblas|| = " << Vartest << std::endl;
    }
    TESTRESULT(Vartest < precision);
  }
#endif

  if (mpiRank == 0) {
    TESTSUMARY;
  }
  if (mpiRank == 0)
    return TESTRETURN;
  else
    return EXIT_SUCCESS;
}

#if defined(FMR_CHAMELEON)
/*
 * This function create 2 random matrices A and B, computes A*B using
 * BlasDenseWrapper and ChameleonDenseWrapper, then Computes A - B to make sure
 * we have the same result. And returns || AB_blas - AB_cham || / || AB_blas ||
 *
 */
template <typename Size, typename Real>
Real cmpBlasCham(const Size nbRowsA, const Size nbColsA, const Size nbColsB,
                 int mpiRank) {

  // Create more readable variables

  const Size nbRowsB = nbColsA;
  const Size nbRowsC = nbRowsA;
  const Size nbColsC = nbColsB;

  // Generate Matrices
  Real *matA = new Real[nbRowsA * nbColsA];
  Real *matB = new Real[nbRowsB * nbColsB];

  std::mt19937_64 generator(std::random_device{}());
  std::normal_distribution<Real> distribution(0.0, 1.0);
  if (mpiRank == 0) {
    for (Size i(0); i < nbRowsA * nbColsA; ++i) {
      matA[i] = distribution(generator);
    }
    for (Size i(0); i < nbRowsB * nbColsB; ++i) {
      matB[i] = distribution(generator);
    }
  }

#if defined(FMR_USE_MPI)
  int mpiinit;
  MPI_Initialized(&mpiinit);
  if ( !(mpiinit) ){
    MPI_Init(NULL, NULL);
  }
  MPI_Datatype dtype = sizeof(Real) == 4 ? MPI_FLOAT : MPI_DOUBLE;
  MPI_Bcast(matA, nbRowsA * nbColsA, dtype, 0, MPI_COMM_WORLD);
  MPI_Bcast(matB, nbRowsB * nbColsB, dtype, 0, MPI_COMM_WORLD);
#endif

  // Create BlasDenseWrapper
  BlasDenseWrapper<Size, Real> ABlas(nbRowsA, nbColsA, matA);
  BlasDenseWrapper<Size, Real> BBlas(nbRowsB, nbColsB, matB);
  BlasDenseWrapper<Size, Real> CBlas(nbRowsC, nbColsC);
  if (mpiRank == 0) {
    fmrGemm(ABlas, BBlas, CBlas);
  }
  // Create ChameleonDenseWrapper
  ChameleonDenseWrapper<Size, Real> ACham(nbRowsA, nbColsA);
  ChameleonDenseWrapper<Size, Real> BCham(nbRowsB, nbColsB);
  ACham.init(matA);
  BCham.init(matB);

  ChameleonDenseWrapper<Size, Real> CCham(nbRowsC, nbColsC);
  fmrGemm(ACham, BCham, CCham);
  // Compare
  Real result(0);

  Real *matCCham = CCham.getBlasMatrix();

  if (mpiRank == 0) {
    Real *matCBlas = CBlas.getBlasMatrix();
    Real normCBlas = computeFrobenius(CBlas);
    for (Size i(0); i < nbRowsC * nbColsC; ++i) {
      matCBlas[i] =
          matCBlas[i] - matCCham[i]; // Store CBlas - CCham inside of CBlas
    }
    Real normCdiff = computeFrobenius(CBlas); // || Cblas - Ccham ||
    result = normCdiff / normCBlas;
  }

  // Finalize
  delete[] matA;
  delete[] matB;
  ABlas.free();
  BBlas.free();
  CBlas.free();
  ACham.free();
  BCham.free();
  CCham.free();
  // Return
  if (mpiRank == 0)
    return result;
  else
    return 0;
}
#endif

/*
 * This function will tests some operations on the Gemm in Blas (trying to
 * transpose some member etc) on known matrices. This is used to validate that
 * the Gemm is correct in Blas before using Blas results as a reference to
 * compare with chameleon
 *
 */
template <typename FReal> bool testBlas(FReal prec, bool verbose) {

  // Create our test matrices
  FReal *a, *b, *c, *d, *e, *f;
  a = new FReal[80];
  b = new FReal[120];
  c = new FReal[100];
  d = new FReal[80];
  e = new FReal[121];
  f = new FReal[90];

  for (int j = 0; j < 10; ++j) {
    for (int i = 0; i < 8; ++i) {
      a[j * 8 + i] = i + j + 1.0 / (i + 1.0);
    }
  }

  for (int j = 0; j < 12; ++j) {
    for (int i = 0; i < 10; ++i) {
      b[j * 10 + i] = (i + 1) * (j + 2) / 2.0;
    }
  }

  for (int j = 0; j < 10; ++j) {
    for (int i = 0; i < 10; ++i) {
      c[j * 10 + i] = (i + j + 1) / (i + 1.0) * 1.0;
    }
  }

  for (int j = 0; j < 10; ++j) {
    for (int i = 0; i < 8; ++i) {
      d[j * 8 + i] = (j - i) / (i + j + 1.0);
    }
  }

  for (int j = 0; j < 13; ++j) {
    for (int i = 0; i < 9; ++i) {
      e[j * 9 + i] = (i + 1) / (j + 1.0);
    }
  }

  for (int j = 0; j < 10; ++j) {
    for (int i = 0; i < 9; ++i) {
      f[j * 9 + i] = (j + 1) / 2.0 * (i + 1.0);
    }
  }

  // AB as calculated with python
  /*
    385.          577.5         770.          962.5         1155. 1347.5 1540.
    1732.5        1925.         2117.5         2310.         2502.5 412.5 618.75
    825.          1031.25       1237.5         1443.75       1650. 1856.25
    2062.5        2268.75        2475.         2681.25 458.33333333  687.5
    916.66666667  1145.83333333 1375.          1604.16666667 1833.33333333
    2062.5        2291.66666667 2520.83333333  2750.         2979.16666667
    508.75        763.125       1017.5        1271.875      1526.25 1780.625
    2035.         2289.375      2543.75       2798.125       3052.5 3306.875
    561.          841.5         1122.         1402.5        1683. 1963.5 2244.
    2524.5        2805.         3085.5         3366.         3646.5 614.16666667
    921.25        1228.33333333 1535.41666667 1842.5         2149.58333333
    2456.66666667 2763.75       3070.83333333 3377.91666667  3685. 3992.08333333
    667.85714286  1001.78571429 1335.71428571 1669.64285714 2003.57142857 2337.5
    2671.42857143 3005.35714286 3339.28571429 3673.21428571  4007.14285714
    4341.07142857 721.875       1082.8125     1443.75       1804.6875 2165.625
    2526.5625     2887.5        3248.4375     3609.375      3970.3125 4331.25
    4692.1875
  */

  FReal ab[] = {
      385.,          412.5,         458.33333333,  508.75,        561.,
      614.16666667,  667.85714286,  721.875,       577.5,         618.75,
      687.5,         763.125,       841.5,         921.25,        1001.78571429,
      1082.8125,     770.,          825.,          916.66666667,  1017.5,
      1122.,         1228.33333333, 1335.71428571, 1443.75,       962.5,
      1031.25,       1145.83333333, 1271.875,      1402.5,        1535.41666667,
      1669.64285714, 1804.6875,     1155.,         1237.5,        1375.,
      1526.25,       1683.,         1842.5,        2003.57142857, 2165.625,
      1347.5,        1443.75,       1604.16666667, 1780.625,      1963.5,
      2149.58333333, 2337.5,        2526.5625,     1540.,         1650.,
      1833.33333333, 2035.,         2244.,         2456.66666667, 2671.42857143,
      2887.5,        1732.5,        1856.25,       2062.5,        2289.375,
      2524.5,        2763.75,       3005.35714286, 3248.4375,     1925.,
      2062.5,        2291.66666667, 2543.75,       2805.,         3070.83333333,
      3339.28571429, 3609.375,      2117.5,        2268.75,       2520.83333333,
      2798.125,      3085.5,        3377.91666667, 3673.21428571, 3970.3125,
      2310.,         2475.,         2750.,         3052.5,        3366.,
      3685.,         4007.14285714, 4331.25,       2502.5,        2681.25,
      2979.16666667, 3306.875,      3646.5,        3992.08333333, 4341.07142857,
      4692.1875};
  // CD**t as calculated with python
  /*45.000001    31.05964     21.032104    13.282802    7.056242    1.9192661
    -2.403139   -6.0969842
    26.0355165   17.5000045   11.508025    6.927599     3.2710905   0.2677072
    -2.25130785 -4.3987074
    19.71402238  12.98012718  8.33333328   4.80919928   2.00937457  -0.28281124
    -2.20069633 -3.83261405
    16.55327425  10.72018675  6.7459855    3.7499975    1.37851475  -0.55807225
    -2.17539228 -3.549569 14.6568258   9.3642232    5.7935776    3.1144772
    0.9999996   -0.72322814 -2.16020916 -3.37974132
    13.39252612  8.46024632   5.15863772   2.69079572   0.74765493  -0.83333326
    -2.15008822 -3.26652395 12.48945459  7.81454919   4.70511028   2.38816734
    0.56741009  -0.91197835 -2.14285752 -3.18565292
    11.81215312  7.33027788   4.36496575   2.16119675   0.43222688  -0.97096197
    -2.13743449 -3.1249998 11.28536329  6.9536241    4.10041132   1.98466685
    0.32708733  -1.0168351  -2.13321353 -3.07782212
    10.8639289   6.6522961    3.8887618    1.8434366    0.2429693   -1.05353992
    -2.12984293 -3.04008596
  */
  FReal cd[] = {
      45.000001,   26.0355165,  19.71402238, 16.55327425, 14.6568258,
      13.39252612, 12.48945459, 11.81215312, 11.28536329, 10.8639289,
      31.05964,    17.5000045,  12.98012718, 10.72018675, 9.3642232,
      8.46024632,  7.81454919,  7.33027788,  6.9536241,   6.6522961,
      21.032104,   11.508025,   8.33333328,  6.7459855,   5.7935776,
      5.15863772,  4.70511028,  4.36496575,  4.10041132,  3.8887618,
      13.282802,   6.927599,    4.80919928,  3.7499975,   3.1144772,
      2.69079572,  2.38816734,  2.16119675,  1.98466685,  1.8434366,
      7.056242,    3.2710905,   2.00937457,  1.37851475,  0.9999996,
      0.74765493,  0.56741009,  0.43222688,  0.32708733,  0.2429693,
      1.9192661,   0.2677072,   -0.28281124, -0.55807225, -0.72322814,
      -0.83333326, -0.91197835, -0.97096197, -1.0168351,  -1.05353992,
      -2.403139,   -2.25130785, -2.20069633, -2.17539228, -2.16020916,
      -2.15008822, -2.14285752, -2.13743449, -2.13321353, -2.12984293,
      -6.0969842,  -4.3987074,  -3.83261405, -3.549569,   -3.37974132,
      -3.26652395, -3.18565292, -3.1249998,  -3.07782212, -3.04008596};

  /* E**TF as calculated with python
     142.5       285.       427.5       570.       712.5        855.       997.5
     1140.       1282.5       1425. 71.25       142.5      213.75      285.
     356.25       427.5      498.75      570.        641.25       712.5
     47.5000035  95.000007  142.5000105 190.000014 237.5000175  285.000021
     332.5000245 380.000028  427.5000315  475.000035 35.625      71.25 106.875
     142.5      178.125      213.75     249.375     285.        320.625 356.25
     28.5        57.        85.5        114.       142.5        171.       199.5
     228.        256.5        285. 23.749998   47.499996  71.249994   94.999992
     118.74999    142.499988 166.249986  189.999984  213.749982   237.49998
     20.357136   40.714272  61.071408   81.428544  101.78568    122.142816
     142.499952  162.857088  183.214224   203.57136
     17.8125     35.625     53.4375     71.25      89.0625      106.875 124.6875
     142.5       160.3125     178.125
     15.833335   31.66667   47.500005   63.33334   79.166675    95.00001
     110.833345  126.66668   142.500015   158.33335 14.25       28.5       42.75
     57.        71.25        85.5       99.75       114.        128.25 142.5
     12.95454755 25.9090951 38.86364265 51.8181902 64.77273775  77.7272853 90.68183285
     103.6363804 116.59092795 129.5454755
     11.87500065 23.7500013 35.62500195 47.5000026 59.37500325  71.2500039 83.12500455
     95.0000052  106.87500585 118.7500065
     10.96153955 21.9230791 32.88461865 43.8461582 54.80769775  65.7692373 76.73077685
     87.6923164  98.65385595  109.6153955
  */

  FReal ef[] = {
      142.5,        71.25,       47.5000035,  35.625,      28.5,
      23.749998,    20.357136,   17.8125,     15.833335,   14.25,
      12.95454755,  11.87500065, 10.96153955, 285.,        142.5,
      95.000007,    71.25,       57.,         47.499996,   40.714272,
      35.625,       31.66667,    28.5,        25.9090951,  23.7500013,
      21.9230791,   427.5,       213.75,      142.5000105, 106.875,
      85.5,         71.249994,   61.071408,   53.4375,     47.500005,
      42.75,        38.86364265, 35.62500195, 32.88461865, 570.,
      285.,         190.000014,  142.5,       114.,        94.999992,
      81.428544,    71.25,       63.33334,    57.,         51.8181902,
      47.5000026,   43.8461582,  712.5,       356.25,      237.5000175,
      178.125,      142.5,       118.74999,   101.78568,   89.0625,
      79.166675,    71.25,       64.77273775, 59.37500325, 54.80769775,
      855.,         427.5,       285.000021,  213.75,      171.,
      142.499988,   122.142816,  106.875,     95.00001,    85.5,
      77.7272853,   71.2500039,  65.7692373,  997.5,       498.75,
      332.5000245,  249.375,     199.5,       166.249986,  142.499952,
      124.6875,     110.833345,  99.75,       90.68183285, 83.12500455,
      76.73077685,  1140.,       570.,        380.000028,  285.,
      228.,         189.999984,  162.857088,  142.5,       126.66668,
      114.,         103.6363804, 95.0000052,  87.6923164,  1282.5,
      641.25,       427.5000315, 320.625,     256.5,       213.749982,
      183.214224,   160.3125,    142.500015,  128.25,      116.59092795,
      106.87500585, 98.65385595, 1425.,       712.5,       475.000035,
      356.25,       285.,        237.49998,   203.57136,   178.125,
      158.33335,    142.5,       129.5454755, 118.7500065, 109.6153955};

  BlasDenseWrapper<int, FReal> A(8, 10);
  BlasDenseWrapper<int, FReal> B(10, 12);
  BlasDenseWrapper<int, FReal> C(10, 10);
  BlasDenseWrapper<int, FReal> D(8, 10);
  BlasDenseWrapper<int, FReal> E(9, 13);
  BlasDenseWrapper<int, FReal> F(9, 10);
  BlasDenseWrapper<int, FReal> AB(8, 12);
  AB.allocate();
  BlasDenseWrapper<int, FReal> CDt(10, 8);
  CDt.allocate();
  BlasDenseWrapper<int, FReal> EtF(13, 10);
  EtF.allocate();

  // We ran this code once to get the values
  // const FReal refAB = frobenius(ab, 8*12);
  // std::cout.precision(std::numeric_limits< FReal >::max_digits10);
  // std::cout << "refAB " << refAB << std::endl;
  // const FReal refCD = frobenius(cd, 10*8);
  // std::cout << "refCD " << refCD << std::endl;
  // const FReal refEF = frobenius(ef, 10*13);
  // std::cout << "refEF " << refEF << std::endl;

  const FReal refAB = 22367.8203;
  const FReal refCD = 87.2430649;
  const FReal refEF = 3504.4397;

  A.init(a);
  B.init(b);
  C.init(c);
  D.init(d);
  E.init(e);
  F.init(f);

  fmrGemm(A, B, AB);
  FReal ABl[8 * 12];
  AB.copyMatrix(ABl);
  for (int i = 0; i < 8 * 12; i++) {
    ABl[i] -= ab[i];
  }
  AB.init(ABl);
  FReal normDiffAb = computeFrobenius(AB);
  if (verbose) {
    std::cout << "||AB  - refAB ||  / ||refAB || =  " << normDiffAb / refAB
              << std::endl;
  }

  if (normDiffAb / refAB > prec)
    return false;

  fmrGemm("N", "t", FReal(1.0), C, D, FReal(0.0), CDt);
  FReal CDtl[10 * 8];
  CDt.copyMatrix(CDtl);

  for (int i = 0; i < 10 * 8; i++) {
    CDtl[i] -= cd[i];
  }
  CDt.init(CDtl);
  FReal normDiffCd = computeFrobenius(CDt);
  if (verbose) {
    std::cout << "||CDt - refCDt||  / ||refCDt|| =  " << normDiffCd / refCD
              << std::endl;
  }

  if (normDiffCd / refCD > prec)
    return false;

  fmrGemm("T", "n", FReal(1.0), E, F, FReal(0.0), EtF);

  FReal EtFl[13 * 10];
  EtF.copyMatrix(EtFl);

  for (int i = 0; i < 10 * 13; i++) {
    EtFl[i] -= ef[i];
  }
  EtF.init(EtFl);
  FReal normDiffEf = computeFrobenius(EtF);
  if (verbose) {
    std::cout << "||EtF - refEtF||  / ||refEtF|| =  " << normDiffEf / refEF
              << std::endl;
  }

  if (normDiffEf / refEF > prec)
    return false;

  delete[] a;
  delete[] b;
  delete[] c;
  delete[] d;
  delete[] e;
  delete[] f;
  return true;
}

// Code used to compute the norm of the reference matrices
// template<typename Real>
// Real frobenius(Real *tab, int l){
//   Real norm = 0.0;
//   for (int i(0); i < l ; ++i){
//     norm += tab[i] * tab[i];
//   }
//   return std::sqrt(norm);
// }
