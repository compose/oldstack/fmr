/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

/**
 * @brief Unit test on computing SVD matrix from a simple well defined
 *
 */

//      fmrincludes
#include "fmr/Utils/Blas.hpp"
#include <iostream>
#include <string>
#include <cmath>

using namespace fmr;

#define LDA M
#define LDU M
#define LDVT N

using namespace std;
void print_matrix( const string &desc, int m, int n, double* a, int lda ) {
        int i, j;
        std::cout << "\n" <<  desc << "\n";
        for( i = 0; i < m; i++ ) {
                for( j = 0; j < n; j++ ) printf( " %6.2f", a[i+j*lda] );
                printf( "\n" );
        }
}

int main(int argc, char* argv[]) {
  (void)argc;
  (void)argv;

  constexpr int M = 6 ;
  constexpr int N = 5 ;

  /* Locals */
  const unsigned int m = M, n = N, lda = LDA, ldu = LDU, ldvt = LDVT ;
  int  info, lwork;
  double wkopt;
  double* work;
  /* Local arrays */
  double s[N], u[LDU*M], vt[LDVT*N];
  double a[LDA*N] = {
    8.79,  6.11, -9.15,  9.57, -3.49,  9.84,
    9.93,  6.91, -7.93,  1.64,  4.02,  0.15,
    9.83,  5.04,  4.86,  8.83,  9.80, -8.99,
    5.45, -0.27,  4.85,  0.74, 10.00, -6.02,
    3.16,  7.98,  3.01,  5.80,  4.27, -5.31
    };

  // Singular values
  double eigenRef[N] = {  27.47 , 22.64 ,  8.56  , 5.99,   2.01} ;

  /* Executable statements */
  std::cout << " DGESVD Example Program Results\n" << std::endl;
  /* Query and allocate the optimal workspace */
  lwork = -1;
  char all[4] =  "All" ;
  Rdgesvd( all, all, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, &wkopt, &lwork,
   &info );
  lwork = (int)wkopt;
  work = (double*)malloc( lwork*sizeof(double) );
  /* Compute SVD */
  Rdgesvd(  all, all, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, work, &lwork,
   &info );
  /* Check for convergence */
  if( info > 0 ) {
          printf( "The algorithm computing SVD failed to converge.\n" );
          exit( 1 );
  }
  /* Print singular values */
  print_matrix( "Singular values", 1, n, s, 1 );
  print_matrix( "Singular values (ref)", 1, n, eigenRef, 1 );
  double error = 0.0 ;
  for ( unsigned int i = 0; i < n ; ++i){
    error += std::abs( s[i] - eigenRef[i]) ;
  }
  std::cout << "Error: " << error << '\n';
  /* Print left singular vectors */
  print_matrix( "Left singular vectors (stored columnwise)", m, n, u, ldu );
  /* Print right singular vectors */
  print_matrix( std::string("Right singular vectors (stored rowwise)"), n, n, vt, ldvt );
  /* Free workspace */
  free( (void*)work );

  return 0;
}
