/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef FMR_EXAMPLES_GENERIC_PRESCRIBEDRANK
#define FMR_EXAMPLES_GENERIC_PRESCRIBEDRANK

#include <iostream>
#include <vector>
//#define FMR_USE_FIXED_SEED

#include <cpp_tools/colors/colorized.hpp>
#include <cpp_tools/cl_parser/help_descriptor.hpp>
#include <cpp_tools/cl_parser/tcli.hpp>

#include "fmr/Algorithms/randomSVD.hpp"    // random projection algorithm
#include "fmr/Algorithms/truncatedSVD.hpp" // random projection algorithm
#include "fmr/Utils/Display.hpp"           // To display matrix or vector
#include "fmr/Utils/RandomMatrix.hpp" // To generate low rank matrix or random matrix
#include "fmr/Utils/MatrixIO.hpp" // To read a matrix
#include "fmr/MatrixWrappers/MatrixWrapperTraits.hpp"
#include "fmr/Utils/MatrixNorms.hpp"

#include "commonParameters.hpp"


///
/// \code
/// ./examples/ex_BlasprescribedRankRandomSVD --nrows 100 --ncols 80
///        --true-rank 40 --p-rank 10 --oversampling 10 --subspace-iter 2
///  \endcode
///
/// \code
/// ./examples/ex_BlasprescribedRankRandomSVD --input-file matrix_1000x1000_f_fast.bin
///     --p_rank 10 --oversampling 0 --subspace_iter 2
/// \endcode
template <class DenseWrapper> int prRSVD( int argc, [[maybe_unused]] char* argv[], const bool masterNode) {
  using value_type = typename DenseWrapper::value_type;
  using int_type = typename DenseWrapper::int_type;

  //
  // Parameter handling
  auto parser = cpp_tools::cl_parser::make_parser(
      cpp_tools::cl_parser::help{}, fmr::parameters::nbrows{},
      fmr::parameters::nbcols{}, fmr::parameters::oversampling{},
      fmr::parameters::nbSubspaceIter{}, fmr::parameters::trank{},
      fmr::parameters::prank{}, fmr::parameters::inputFile{});

  parser.parse(argc, argv);
  //
  int verbose = 0;
  bool readfile = false;
  ///
  /// Define a matrix of rank true_rank
  ///
  DenseWrapper A;

  // Getting command line parameters
  int_type true_rank{-1};
  int_type n{0}, p{0};
  if (parser.template exists<fmr::parameters::inputFile>()) {
    std::string filename = parser.template get<fmr::parameters::inputFile>();
    std::cout << cpp_tools::colors::blue << "read matrix from file " << filename
              << cpp_tools::colors::reset << std::endl;
    value_type *data;
    if constexpr (fmr::traits::is_blas_wrapper<DenseWrapper>::value) {
      fmr::io::readBinary(filename, n, p, data, verbose);
      A.init(n, p, data);
      readfile = true;
    } else {
      throw std::runtime_error(
          "input-file works only with BlasDenseWrapper !!\n");
    }
  } else {
    // The true rank of the generated matrix
    true_rank = parser.get<fmr::parameters::trank>();
    // The number of rows of the matrix
    n = parser.get<fmr::parameters::nbrows>();
    // The number of cols of the matrix
    p = parser.get<fmr::parameters::nbcols>();
    A.initAndAllocate(n, p);
  }
  // The prescribed rank
  const int_type rank{parser.get< fmr::parameters::prank>()} ;
  // The over sampling parameter
  int_type os{parser.get<fmr::parameters::oversampling>()};
  // The nunber of Subspace Iteration
  int_type nbPowerIterations{parser.get<fmr::parameters::nbSubspaceIter>()};


  //
  if (rank + os > p) {
    os = p - rank;
  }

  if (masterNode) {
    std::cout << cpp_tools::colors::blue <<"truncated random SVD parameters\n";
    std::cout << "      Nrows             " << n << std::endl
              << "      Ncols             " << p << std::endl;
    std::cout << "      Rank              " << rank << std::endl;
    std::cout << "      Oversampling      " << os << std::endl;
    std::cout << "      nbPowerIterations " << nbPowerIterations << cpp_tools::colors::reset<< std::endl;
  }
  if (rank > std::min(p, n)) {
    throw std::invalid_argument(
        "Error: rank > std::min(p, n) but should be lower\n");
  }
  if(! readfile){
  ///
  /// Generate a low rank matrix
  fmr::tools::RandomClass<value_type> randGen;
  std::vector<value_type> diag;

  randGen.buildMatrixWithRank(true_rank, A, diag, true);
  }
  auto normA = fmr::computeFrobenius(A);
  std::cout <<cpp_tools::colors::red<< "      Frobenius norm    " <<normA << cpp_tools::colors::reset<< std::endl;

  ///
  ///
  std::vector<value_type> sigma;
  ////////////////////////////////////////////////////
  /// Perform SVD
  ///
 std::cout << "\n###############################################\n";
  std::cout << "#####          truncated SVD              #####\n";
  std::cout << "###############################################\n";
  DenseWrapper *U_ori = nullptr;
  DenseWrapper *VT_ori = nullptr;
  fmr::truncatedSVD(rank, A, sigma, U_ori, VT_ori);
  std::cout << " sigma size " << sigma.size() << std::endl;
  fmr::Display::vector(sigma, " (SVD) sigma S ", sigma.size() );
//  value_type error = 0;
//  for (std::size_t i = 0; i < 2*rank; ++i) {
//    error = std::max(error, std::abs(diag[i] - sigma[i]));
//  }
//  std::cout << cpp_tools::colors::red<< "Error on singular values: "
//            <<error << cpp_tools::colors::reset<< std::endl;
//  if (error / diag[0] > 100 * std::numeric_limits<value_type>::epsilon()) {
//    std::clog << " std::numeric_limits<value_type>::epsilon() "
//              << std::numeric_limits<value_type>::epsilon() << std::endl;
//    throw std::runtime_error("Error on SVD with singular values: " +
//                             std::to_string(error) + "\n Bad results\n");
//  }
  ////////////////////////////////////////////////////
  /// Perform random SVD with precribed rank
  ///
  std::cout << "\n###############################################\n";
  std::cout << "#####            Random SVD               #####\n";
  std::cout << "###############################################\n";

  std::vector<value_type> ApproxSingularValues(rank);
  DenseWrapper *U = nullptr, *VT = nullptr;
  value_type energy, estimator;

  fmr::randomSVD<value_type, DenseWrapper>(rank, os, nbPowerIterations, A,
                                           ApproxSingularValues, U, VT, energy,
                                           estimator, verbose);

  fmr::Display::vector(ApproxSingularValues, "randomSVD S ",
                       ApproxSingularValues.size());

  auto shapeU = U->getShape();
  std::cout << "[fmr] U: " << shapeU[0] << " x " << shapeU[1] << "  "
            << A.getNbRows() << "  " << rank << std::endl;
  std::cout << "Norm of the approximation: " << std::sqrt(energy) << " error: " << std::abs(normA - std::sqrt(energy)) <<std::endl
            << "Fist truncated singular value " << estimator << std::endl;
  value_type error{value_type(0.0)};
  for (std::size_t i = 0; i < rank; ++i) {
    error = std::max(error, std::abs(sigma[i] - ApproxSingularValues[i]));
  }
  std::cout << cpp_tools::colors::red<< "Error on singular values: "
            << error << cpp_tools::colors::reset<< std::endl;

  return EXIT_SUCCESS;
}
#endif
