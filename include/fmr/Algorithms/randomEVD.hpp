/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef RANDOMEVD_HPP
#define RANDOMEVD_HPP

#include <cstdlib>
#include <iostream>
#include <vector>

#include "fmr/RandomizedLRA/AdaptiveRandomizedRangeFinder.hpp"
#include "fmr/RandomizedLRA/RandEVD.hpp"
#include "fmr/RandomizedLRA/RandomizedRangeFinder.hpp"
#include "fmr/StandardLRA/EVD.hpp"

namespace fmr{
  //////
  /// \brief Compute a random projection EVD for a given rank
  ///
  /// \param[in] prescribed_rank the number of eigenvalues required
  /// \param[in] oversampling  Oversampling using in the random projection
  /// \param[in] nbPowerIterations number of iteration of the power iteration
  /// \param[in] A a symmetric matrix with n rows and n cols
  /// \param[out] lambda the eigenvalues vector of size prescribed_rank sorted in decreasing order
  /// \param[out] U orthogonal matrix of size  n x prescribed_rank
  /// \param[out] energy2 is the sum of the squared eigeneigenvalues. This corresponds to an estimate of the squared norm of the matrix A (not yet implemented)
  /// \param[out] estimator is the first truncated eigenvalues (if oversampling > 0), (not yet implemented)
  ///

  template <typename REAL_T, class MATRIX_T>
  void randomEVD(const int &prescribed_rank, const int &oversampling,
                 const int &nbPowerIterations, MATRIX_T &A,
                 std::vector<REAL_T> &lambda, MATRIX_T &U,
                 REAL_T &energy2, REAL_T &estimator, const int verbose = 0) {

    (void)energy2;
    (void)estimator;
    REAL_T *ApproxEigenValues = nullptr;
    using SIZE_T = typename MATRIX_T::int_type;

    if (verbose > 0) {
      std::cout << "[fmr][randomEVD] Prescribed rank: " <<
        prescribed_rank << std::endl;
      std::cout << "[fmr][randomEVD] Oversampling:    " << oversampling << std::endl;
      std::cout << "[fmr][randomEVD] Nb of subspace iterations: " << nbPowerIterations
                << std::endl;
    }

    if ((SIZE_T)(prescribed_rank + oversampling) > A.getNbCols())
      throw std::runtime_error(
            "[fmr][randomEVD] Oversampled rank should be lower than full rank!");


    using Dense_RRFClass = RandomizedRangeFinder<REAL_T, MATRIX_T>;
    //
    // Classic Randomized Range Finder
    // RRF
    Dense_RRFClass RandRangeFinder(&A, prescribed_rank, oversampling,
                                   nbPowerIterations);

    RandEVD<REAL_T, MATRIX_T, Dense_RRFClass>REVD(A.getNbRows(),
                                                 &RandRangeFinder);

    auto rank = REVD.factorize(verbose);
    ApproxEigenValues = REVD.getApproxEigenValues();
    lambda.assign(ApproxEigenValues, ApproxEigenValues + rank);

    // Get approx. eigen vectors. Reshape the matrix to ignore columns related
    // to oversampling
    MATRIX_T* Uover = REVD.getApproxU();
    U.init(A.getNbRows(), rank, *Uover);

    const auto shapeU = REVD.getShapeU();

    // TODO : get energy2 and ApproxBaselineSpec ?
    if (verbose > 0) {
      std::cout << "[fmr] U: " << shapeU[0] << " x " << shapeU[1] << "  "
                << A.getNbRows() << "  " << rank << std::endl;
      // std::cout << "[fmr] energy2: " << energy2 << "  estimator " << estimator
      //           << std::endl;
    }
  }

} /* namespace fmr */

#endif /* RANDOMEVD_HPP */
