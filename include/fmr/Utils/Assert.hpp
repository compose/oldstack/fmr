/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

// ===================================================================================
// Copyright ScalFmm 2016 INRIA, Olivier Coulaud, Bérenger Bramas,
// Matthias Messner olivier.coulaud@inria.fr, berenger.bramas@inria.fr
// This software is a computer program whose purpose is to compute the
// FMM.
//
// This software is governed by the CeCILL-C and LGPL licenses and
// abiding by the rules of distribution of free software.
// An extension to the license is given to allow static linking of scalfmm
// inside a proprietary application (no matter its license).
// See the main license file for more details.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public and CeCILL-C Licenses for more details.
// "http://www.cecill.info".
// "http://www.gnu.org/licenses".
// ===================================================================================
#ifndef ASSERT_HPP
#define ASSERT_HPP

#include <iostream>
#include <cassert>

#include "fmr/Utils/Global.hpp"

namespace fmr{

/**
 * @brief The Error class
 * It is recommended to use the macro:
 * AssertLF( aTest , "some data ", "to ", plot);
 * Assertion are enabled or disabled during the compilation.
 * If disabled, the test instruction is still used (but the return will be optimized out by
 * the compiler).
 */
class Error {
protected:
    /**
     * @brief ErrPrint private method to end print
     */
    static void ErrPrint(){
        std::cerr << '\n';
    }

    /**
     * @brief ErrPrint private methdo to print
     */
    template<typename T, typename... Args>
    static void ErrPrint(const T& toPrint, Args... args){
        std::cerr << toPrint;
        ErrPrint( args... );
    }


public:
    //////////////////////////////////////////////////////////////
    // Should not be called directly
    //////////////////////////////////////////////////////////////

    /** Nothing to print */
    static void Print(){
    }

    /** One or more things to print */
    template<typename T, typename... Args>
    static void Print(const T& toPrint, Args... args){
        std::cerr << "[ERROR] ";
        ErrPrint( toPrint, args... );
    }
};

#ifdef FMR_USE_ASSERT

//////////////////////////////////////////////////////////////
// Sp error activated
//////////////////////////////////////////////////////////////

#define ErrorAssertExit(TEST, ...) \
    if( !(TEST) ){ \
        Error::Print( __VA_ARGS__ ); \
        throw std::exception(); \
    }
#else

//////////////////////////////////////////////////////////////
// Sp error desactivated
//////////////////////////////////////////////////////////////

#define ErrorAssertExit(TEST, ...) \
    if( !(TEST) ){}
#endif

//////////////////////////////////////////////////////////////
// Shortcut macro
//////////////////////////////////////////////////////////////

#define MY_ERROR_LINE " At line " , __LINE__ , "."
#define MY_ERROR_FILE " In file " , __FILE__ , "."

#define Assert ErrorAssertExit

#define AssertLF(...) Assert(__VA_ARGS__, MY_ERROR_LINE, MY_ERROR_FILE)

} /* namespace fmr */

#endif //ASSERT_HPP


