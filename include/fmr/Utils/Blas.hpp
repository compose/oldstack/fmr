/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef BLAS_HPP
#define BLAS_HPP

#include <cstdio>
#include <cstdlib>
//#include "fmr/Utils/Global.hpp"
#include "FCMangling.hpp"

// This file interfaces the blas functions to enable a generic use.

// First handle the Fortran Mangling...

//#ifdef BLAS_ADD_
///* Mangling for Fortran subroutine symbols with underscores. */

//#define FortranName(name,NAME) name##_

//#elif defined(BLAS_UPCASE)

///* Mangling for Fortran subroutine  symbols in uppercase and without
/// underscores */

//#define FortranName(name,NAME) NAME

//#elif defined(BLAS_NOCHANGE)
///* Mangling for Fortran subroutine  symbols without no change. */

//#define FortranName(name,NAME) name

//#else

//#error("Fortran MANGLING NOT DEFINED for Blas")

//#endif

// blas 1
#define Rddot FortranName_GLOBAL(ddot, DDOT)
#define Rdscal FortranName_GLOBAL(dscal, DSCAL)
#define Rdcopy FortranName_GLOBAL(dcopy, DCOPY)
#define Rdaxpy FortranName_GLOBAL(daxpy, DAXPY)
#define Rsdot FortranName_GLOBAL(sdot, SDOT)
#define Rsscal FortranName_GLOBAL(sscal, SSCAL)
#define Rscopy FortranName_GLOBAL(scopy, SCOPY)
#define Rsaxpy FortranName_GLOBAL(saxpy, SAXPY)
#define Rcscal FortranName_GLOBAL(cscal, CSCAL)
#define Rccopy FortranName_GLOBAL(ccopy, CCOPY)
#define Rcaxpy FortranName_GLOBAL(caxpy, CAXPY)
#define Rzscal FortranName_GLOBAL(zscal, ZSCAL)
#define Rzcopy FortranName_GLOBAL(zcopy, ZCOPY)
#define Rzaxpy FortranName_GLOBAL(zaxpy, ZAXPY)
// blas 2
#define Rdgemv FortranName_GLOBAL(dgemv, DGEMV)
#define Rsgemv FortranName_GLOBAL(sgemv, SGEMV)
#define Rcgemv FortranName_GLOBAL(cgemv, CGEMV)
#define Rzgemv FortranName_GLOBAL(zgemv, ZGEMV)
#define Rdsymv FortranName_GLOBAL(dsymv, DSYMV)
#define Rssymv FortranName_GLOBAL(ssymv, SSYMV)
#define Rcsymv FortranName_GLOBAL(csymv, CSYMV)
#define Rzsymv FortranName_GLOBAL(zsymv, ZSYMV)
// blas 3
#define Rdgemm FortranName_GLOBAL(dgemm, DGEMM)
#define Rsgemm FortranName_GLOBAL(sgemm, SGEMM)
#define Rcgemm FortranName_GLOBAL(cgemm, CGEMM)
#define Rzgemm FortranName_GLOBAL(zgemm, ZGEMM)

#define Rdgemm_alloc FortranName_GLOBAL(dgemm_alloc, DGEMM_ALLOC)
#define Rsgemm_alloc FortranName_GLOBAL(sgemm_alloc, SGEMM_ALLOC)
#define Rcgemm_alloc FortranName_GLOBAL(cgemm_alloc, CGEMM_ALLOC)
#define Rzgemm_alloc FortranName_GLOBAL(zgemm_alloc, ZGEMM_ALLOC)
#define Rdgemm_pack FortranName_GLOBAL(dgemm_pack, DGEMM_PACK)
#define Rsgemm_pack FortranName_GLOBAL(sgemm_pack, SGEMM_PACK)
#define Rcgemm_pack FortranName_GLOBAL(cgemm_pack, CGEMM_PACK)
#define Rzgemm_pack FortranName_GLOBAL(zgemm_pack, ZGEMM_PACK)
#define Rdgemm_compute FortranName_GLOBAL(dgemm_compute, DGEMM_COMPUTE)
#define Rsgemm_compute FortranName_GLOBAL(sgemm_compute, SGEMM_COMPUTE)
#define Rcgemm_compute FortranName_GLOBAL(cgemm_compute, CGEMM_COMPUTE)
#define Rzgemm_compute FortranName_GLOBAL(zgemm_compute, ZGEMM_COMPUTE)
#define Rdgemm_free FortranName_GLOBAL(dgemm_free, DGEMM_FREE)
#define Rsgemm_free FortranName_GLOBAL(sgemm_free, SGEMM_FREE)
#define Rcgemm_free FortranName_GLOBAL(cgemm_free, CGEMM_FREE)
#define Rzgemm_free FortranName_GLOBAL(zgemm_free, ZGEMM_FREE)

//
#define Rdsymm FortranName_GLOBAL(dsymm, DSYMM)
#define Rssymm FortranName_GLOBAL(ssymm, SSYMM)
#define Rcsymm FortranName_GLOBAL(csymm, CSYMM)
#define Rzsymm FortranName_GLOBAL(zsymm, ZSYMM)
// lapack
#define Rdgesvd FortranName_GLOBAL(dgesvd, DGESVD)
#define Rdgeqrf FortranName_GLOBAL(dgeqrf, DGEQRF)
#define Rdgeqp3 FortranName_GLOBAL(dgeqp3, DGEQP3)
#define Rdorgqr FortranName_GLOBAL(dorgqr, DORGQR)
#define Rdormqr FortranName_GLOBAL(dormqr, DORMQR)
#define Rdpotrf FortranName_GLOBAL(dpotrf, DPOTRF)

#define Rdgeev FortranName_GLOBAL(dgeev, DGEEV)
#define Rsgeev FortranName_GLOBAL(sgeev, SGEEV)
#define Rcgeev FortranName_GLOBAL(cgeev, CGEEV)
#define Rzgeev FortranName_GLOBAL(zgeev, ZGEEV)

#define Rdsyev FortranName_GLOBAL(dsyev, DSYEV)
#define Rssyev FortranName_GLOBAL(ssyev, SSYEV)
#define Rcheev FortranName_GLOBAL(cheev, CHEEV)
#define Rzheev FortranName_GLOBAL(zheev, ZHEEV)

#define Rsgesvd FortranName_GLOBAL(sgesvd, SGESVD)
#define Rsgeqrf FortranName_GLOBAL(sgeqrf, SGEQRF)
#define Rsgeqp3 FortranName_GLOBAL(sgeqp3, SGEQP3)
#define Rsorgqr FortranName_GLOBAL(sorgqr, SORGQR)
#define Rsormqr FortranName_GLOBAL(sormqr, SORMQR)
#define Rspotrf FortranName_GLOBAL(spotrf, SPOTRF)

#define Rcgesvd FortranName_GLOBAL(cgesvd, CGESVD)
#define Rcgeqrf FortranName_GLOBAL(cgeqrf, CGEQRF)
#define Rcgeqp3 FortranName_GLOBAL(cgeqp3, CGEQP3)
#define Rcorgqr FortranName_GLOBAL(corgqr, CORGQR)
#define Rcormqr FortranName_GLOBAL(cormqr, CORMQR)
#define Rcpotrf FortranName_GLOBAL(cpotrf, CPOTRF)

#define Rzgesvd FortranName_GLOBAL(zgesvd, ZGESVD)
#define Rzgeqrf FortranName_GLOBAL(zgeqrf, ZGEQRF)
#define Rzgeqp3 FortranName_GLOBAL(zgeqp3, ZGEQP3)
#define Rzorgqr FortranName_GLOBAL(zorgqr, ZORGQR)
#define Rzormqr FortranName_GLOBAL(zormqr, ZORMQR)
#define Rzpotrf FortranName_GLOBAL(zpotrf, ZPOTRF)

// Then define som constants
namespace fmr {
const double D_ZERO = 0.0;
const double D_ONE = 1.0;
const double D_MONE = -1.0;
const float S_ZERO = 0.0;
const float S_ONE = 1.0;
const float S_MONE = -1.0;
// for complex
const double Z_ZERO[2] = {0.0, 0.0};
const double Z_ONE[2] = {1.0, 0.0};
const double Z_MONE[2] = {-1.0, 0.0};
const float C_ZERO[2] = {0.0, 0.0};
const float C_ONE[2] = {1.0, 0.0};
const float C_MONE[2] = {-1.0, 0.0};

// const double D_PREC = 1e-16;

const unsigned N_ONE = 1;
const int N_MONE = -1;
const char JOB_STR[] = "NTOSVULCRPA";
} // namespace fmr

// Declare functions
extern "C" {
// double //////////////////////////////////////////////////////////
// blas 1
double Rddot(const unsigned *, const double *, const unsigned *, const double *,
             const unsigned *);
void Rdscal(const unsigned *, const double *, const double *, const unsigned *);
void Rdcopy(const unsigned *, const double *, const unsigned *, double *,
            const unsigned *);
void Rdaxpy(const unsigned *, const double *, const double *, const unsigned *,
            double *, const unsigned *);
// blas 2
void Rdgemv(const char *, const unsigned *, const unsigned *, const double *,
            const double *, const unsigned *, const double *, const unsigned *,
            const double *, double *, const unsigned *);
void Rdsymv(const char *, const unsigned *, const double *, const double *,
            const unsigned *, const double *, const unsigned *, const double *,
            double *, const unsigned *);
// blas 3
void Rdgemm(const char *, const char *, const unsigned *, const unsigned *,
            const unsigned *, const double *, double *, const unsigned *,
            double *, const unsigned *, const double *, double *,
            const unsigned *);
double *Rdgemm_alloc(const char *, const unsigned *, const unsigned *,
                     const unsigned *);
void Rdgemm_pack(const char *, const char *, const unsigned *, const unsigned *,
                 const unsigned *, const double *, const double *,
                 const unsigned *, double *);
void Rdgemm_compute(const char *, const char *, const unsigned *,
                    const unsigned *, const unsigned *, double *,
                    const unsigned *, double *, const unsigned *,
                    const double *, double *, const unsigned *);
void Rdgemm_free(double *);

void Rdsymm(const char *, const char *, const unsigned *, const unsigned *,
            const double *, double *, const unsigned *, double *,
            const unsigned *, const double *, double *, const unsigned *);

// lapack
void Rdgesvd(const char *, const char *, const unsigned *, const unsigned *,
             double *, const unsigned *, double *, double *, const unsigned *,
             double *, const unsigned *, double *, const int *, int *);
void Rdgeqrf(const unsigned *, const unsigned *, double *, const unsigned *,
             double *, double *, const unsigned *, int *);
void Rdgeqp3(const unsigned *, const unsigned *, double *, const unsigned *,
             /*TYPE OF JPIV*/ unsigned *, double *, double *, const unsigned *,
             int *);
void Rdorgqr(const unsigned *, const unsigned *, const unsigned *, double *,
             const unsigned *, double *, double *, const unsigned *, int *);
void Rdormqr(const char *, const char *, const unsigned *, const unsigned *,
             const unsigned *, const double *, const unsigned *, double *,
             double *, const unsigned *, double *, const unsigned *, int *);
void Rdpotrf(const char *, const unsigned *, double *, const unsigned *, int *);
// dgeev ressemble alot dgesvd in term of proto except that the matrix has to be
// square so only 1 size
void Rdgeev(const char *, const char *, const unsigned *, double *,
            const unsigned *, double *, double *, double *, const unsigned *,
            double *, const unsigned *, double *, const int *, int *);
void Rdsyev(const char * /*JOBZ*/, const char * /*UPLO*/, const unsigned *,
            double *, const unsigned *, double *, double * /*WORK*/,
            const int * /*LWORK*/, int * /*INFO*/);

// single //////////////////////////////////////////////////////////
// blas 1
float Rsdot(const unsigned *, const float *, const unsigned *, const float *,
            const unsigned *);
void Rsscal(const unsigned *, const float *, const float *, const unsigned *);
void Rscopy(const unsigned *, const float *, const unsigned *, float *,
            const unsigned *);
void Rsaxpy(const unsigned *, const float *, const float *, const unsigned *,
            float *, const unsigned *);
// blas 2
void Rsgemv(const char *, const unsigned *, const unsigned *, const float *,
            const float *, const unsigned *, const float *, const unsigned *,
            const float *, float *, const unsigned *);
void Rssymv(const char *, const unsigned *, const float *, const float *,
            const unsigned *, const float *, const unsigned *, const float *,
            float *, const unsigned *);
// blas 3
void Rsgemm(const char *, const char *, const unsigned *, const unsigned *,
            const unsigned *, const float *, float *, const unsigned *, float *,
            const unsigned *, const float *, float *, const unsigned *);
float *Rsgemm_alloc(const char *, const unsigned *, const unsigned *,
                    const unsigned *);
void Rsgemm_pack(const char *, const char *, const unsigned *, const unsigned *,
                 const unsigned *, const float *, const float *,
                 const unsigned *, float *);
void Rsgemm_compute(const char *, const char *, const unsigned *,
                    const unsigned *, const unsigned *, float *,
                    const unsigned *, float *, const unsigned *, const float *,
                    float *, const unsigned *);
void Rsgemm_free(float *);

void Rssymm(const char *, const char *, const unsigned *, const unsigned *,
            const float *, float *, const unsigned *, float *, const unsigned *,
            const float *, float *, const unsigned *);
// lapack
void Rsgesvd(const char *, const char *, const unsigned *, const unsigned *,
             float *, const unsigned *, float *, float *, const unsigned *,
             float *, const unsigned *, float *, const int *, int *);
void Rsgeqrf(const unsigned *, const unsigned *, float *, const unsigned *,
             float *, float *, const unsigned *, int *);
void Rsgeqp3(const unsigned *, const unsigned *, float *, const unsigned *,
             /*TYPE OF JPIV*/ unsigned *, float *, float *, const unsigned *,
             int *);
void Rsorgqr(const unsigned *, const unsigned *, const unsigned *, float *,
             const unsigned *, float *, float *, const unsigned *, int *);
void Rsormqr(const char *, const char *, const unsigned *, const unsigned *,
             const unsigned *, const float *, const unsigned *, float *,
             float *, const unsigned *, float *, const unsigned *, int *);
void Rspotrf(const char *, const unsigned *, float *, const unsigned *, int *);
void Rsgeev(const char *, const char *, const unsigned *, float *,
            const unsigned *, float *, float *, float *, const unsigned *,
            float *, const unsigned *, float *, const int *, const int *);
void Rssyev(const char * /*JOBZ*/, const char * /*UPLO*/, const unsigned *,
            float *, const unsigned *, float *,
            float * /*WORK*/, const int * /*LWORK*/, int * /*INFO*/);

// double complex //////////////////////////////////////////////////
// blas 1
void Rzscal(const unsigned *, const double *, const double *, const unsigned *);
void Rzcopy(const unsigned *, const double *, const unsigned *, double *,
            const unsigned *);
void Rzaxpy(const unsigned *, const double *, const double *, const unsigned *,
            double *, const unsigned *);
// blas 2
void Rzgemv(const char *, const unsigned *, const unsigned *, const double *,
            const double *, const unsigned *, const double *, const unsigned *,
            const double *, double *, const unsigned *);
void Rzsymv(const char *, const unsigned *, const double *, const double *,
            const unsigned *, const double *, const unsigned *, const double *,
            double *, const unsigned *);
// blas 3
void Rzgemm(const char *, const char *, const unsigned *, const unsigned *,
            const unsigned *, const double *, double *, const unsigned *,
            double *, const unsigned *, const double *, double *,
            const unsigned *);
double *Rzgemm_alloc(const char *, const unsigned *, const unsigned *,
                     const unsigned *);
void Rzgemm_pack(const char *, const char *, const unsigned *, const unsigned *,
                 const unsigned *, const double *, const double *,
                 const unsigned *, double *);
void Rzgemm_compute(const char *, const char *, const unsigned *,
                    const unsigned *, const unsigned *, double *,
                    const unsigned *, double *, const unsigned *,
                    const double *, double *, const unsigned *);
void Rzgemm_free(double *);

void Rzsymm(const char *, const char *, const unsigned *, const unsigned *,
            const double *, double *, const unsigned *, double *,
            const unsigned *, const double *, double *, const unsigned *);
// lapack
void Rzgesvd(const char *, const char *, const unsigned *, const unsigned *,
             double *, const unsigned *, double *, double *, const unsigned *,
             double *, const unsigned *, double *, int *, double *, int *);

void Rzgeqrf(const unsigned *, const unsigned *, double *, const unsigned *,
             double *, double *, const unsigned *, int *);
void Rzgeqp3(const unsigned *, const unsigned *, double *, const unsigned *,
             /*TYPE OF JPIV*/ unsigned *, double *, double *, const unsigned *,
             int *);
void Rzpotrf(const char *, const unsigned *, double *, const unsigned *, int *);
void Rzgeev(const char *, const char *, const unsigned *, double *,
            const unsigned *, double *, double *, double *, const unsigned *,
            double *, const unsigned *, double *, const int *, int *);
void Rzheev(const char * /*JOBZ*/, const char * /*UPLO*/, const unsigned *,
            double *, const unsigned *, double *, double * /*WORK*/,
            const int * /*LWORK*/, double * /*RWORK*/, int * /*INFO*/);

// single complex //////////////////////////////////////////////////
// blas 1
void Rcscal(const unsigned *, const float *, const float *, const unsigned *);
void Rccopy(const unsigned *, const float *, const unsigned *, float *,
            const unsigned *);
void Rcaxpy(const unsigned *, const float *, const float *, const unsigned *,
            float *, const unsigned *);
// blas 2
void Rcgemv(const char *, const unsigned *, const unsigned *, const float *,
            const float *, const unsigned *, const float *, const unsigned *,
            const float *, float *, const unsigned *);
void Rcsymv(const char *, const unsigned *, const float *, const float *,
            const unsigned *, const float *, const unsigned *, const float *,
            float *, const unsigned *);
// blas 3
void Rcgemm(const char *, const char *, const unsigned *, const unsigned *,
            const unsigned *, const float *, float *, const unsigned *, float *,
            const unsigned *, const float *, float *, const unsigned *);
float *Rcgemm_alloc(const char *, const unsigned *, const unsigned *,
                    const unsigned *);
void Rcgemm_pack(const char *, const char *, const unsigned *, const unsigned *,
                 const unsigned *, const float *, const float *,
                 const unsigned *, float *);
void Rcgemm_compute(const char *, const char *, const unsigned *,
                    const unsigned *, const unsigned *, float *,
                    const unsigned *, float *, const unsigned *, const float *,
                    float *, const unsigned *);
void Rcgemm_free(float *);

void Rcsymm(const char *, const char *, const unsigned *, const unsigned *,
            const float *, float *, const unsigned *, float *, const unsigned *,
            const float *, float *, const unsigned *);
// lapack
void Rcgeqrf(const unsigned *, const unsigned *, float *, const unsigned *,
             float *, float *, const unsigned *, int *);
void Rcgeqp3(const unsigned *, const unsigned *, float *, const unsigned *,
             /*TYPE OF JPIV*/ unsigned *, float *, float *, const unsigned *,
             int *);
void Rcpotrf(const char *, const unsigned *, float *, const unsigned *, int *);
void Rcgeev(const char *, const char *, const unsigned *, float *,
            const unsigned *, float *, float *, float *, const unsigned *,
            float *, const unsigned *, float *, const int *, int *);
void Rcheev(const char * /*JOBZ*/, const char * /*UPLO*/, const unsigned *,
            float *, const unsigned *, float *, float * /*WORK*/,
            const int * /*LWORK*/, float * /*RWORK*/, int * /*INFO*/);
}

// Finally define functions
namespace Blas {

// copy
inline void copy(const unsigned n, double *orig, double *dest) {
  Rdcopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}
inline void copy(const unsigned n, const double *orig, double *dest) {
  Rdcopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}
inline void copy(const unsigned n, float *orig, float *dest) {
  Rscopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}
inline void copy(const unsigned n, const float *orig, float *dest) {
  Rscopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}
inline void c_copy(const unsigned n, double *orig, double *dest) {
  Rzcopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}
inline void c_copy(const unsigned n, const double *orig, double *dest) {
  Rzcopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}
inline void c_copy(const unsigned n, float *orig, float *dest) {
  Rccopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}
inline void c_copy(const unsigned n, const float *orig, float *dest) {
  Rccopy(&n, orig, &fmr::N_ONE, dest, &fmr::N_ONE);
}

// copy (variable increment)
inline void copy(const unsigned n, double *orig, const unsigned inco,
                 double *dest, const unsigned incd) {
  Rdcopy(&n, orig, &inco, dest, &incd);
}
inline void copy(const unsigned n, float *orig, const unsigned inco,
                 float *dest, const unsigned incd) {
  Rscopy(&n, orig, &inco, dest, &incd);
}
inline void c_copy(const unsigned n, double *orig, const unsigned inco,
                   double *dest, const unsigned incd) {
  Rzcopy(&n, orig, &inco, dest, &incd);
}
inline void c_copy(const unsigned n, float *orig, const unsigned inco,
                   float *dest, const unsigned incd) {
  Rccopy(&n, orig, &inco, dest, &incd);
}

// scale
inline void scal(const unsigned n, const double d, double *const x) {
  Rdscal(&n, &d, x, &fmr::N_ONE);
}
inline void scal(const unsigned n, const float d, float *const x) {
  Rsscal(&n, &d, x, &fmr::N_ONE);
}
inline void c_scal(const unsigned n, const double d, double *const x) {
  Rzscal(&n, &d, x, &fmr::N_ONE);
}
inline void c_scal(const unsigned n, const float d, float *const x) {
  Rcscal(&n, &d, x, &fmr::N_ONE);
}

// scale (variable increment)
inline void scal(const unsigned n, const double d, double *const x,
                 const unsigned incd) {
  Rdscal(&n, &d, x, &incd);
}
inline void scal(const unsigned n, const float d, float *const x,
                 const unsigned incd) {
  Rsscal(&n, &d, x, &incd);
}
inline void c_scal(const unsigned n, const double d, double *const x,
                   const unsigned incd) {
  Rzscal(&n, &d, x, &incd);
}
inline void c_scal(const unsigned n, const float d, float *const x,
                   const unsigned incd) {
  Rcscal(&n, &d, x, &incd);
}

// set zero
inline void setzero(const unsigned n, double *const x) {
  for (unsigned i = 0; i < n; ++i)
    x[i] = 0.0;
}
inline void setzero(const unsigned n, float *const x) {
  for (unsigned i = 0; i < n; ++i)
    x[i] = 0.0f;
}
inline void c_setzero(const unsigned n, double *const x) {
  for (unsigned i = 0; i < n; ++i)
    x[i * 2] = x[i * 2 + 1] = 0.0;
}
inline void c_setzero(const unsigned n, float *const x) {
  for (unsigned i = 0; i < n; ++i)
    x[i * 2] = x[i * 2 + 1] = 0.0f;
}

// y += x
inline void add(const unsigned n, double *const x, double *const y) {
  Rdaxpy(&n, &fmr::D_ONE, x, &fmr::N_ONE, y, &fmr::N_ONE);
}
inline void add(const unsigned n, float *const x, float *const y) {
  Rsaxpy(&n, &fmr::S_ONE, x, &fmr::N_ONE, y, &fmr::N_ONE);
}
inline void c_add(const unsigned n, float *const x, float *const y) {
  Rcaxpy(&n, fmr::C_ONE, x, &fmr::N_ONE, y, &fmr::N_ONE);
}
inline void c_add(const unsigned n, double *const x, double *const y) {
  Rzaxpy(&n, fmr::Z_ONE, x, &fmr::N_ONE, y, &fmr::N_ONE);
}

// y += d x
inline void axpy(const unsigned n, const double d, const double *const x,
                 double *const y) {
  Rdaxpy(&n, &d, x, &fmr::N_ONE, y, &fmr::N_ONE);
}
inline void axpy(const unsigned n, const float d, const float *const x,
                 float *const y) {
  Rsaxpy(&n, &d, x, &fmr::N_ONE, y, &fmr::N_ONE);
}
inline void c_axpy(const unsigned n, const float *d, const float *const x,
                   float *const y) {
  Rcaxpy(&n, d, x, &fmr::N_ONE, y, &fmr::N_ONE);
}
inline void c_axpy(const unsigned n, const double *d, const double *const x,
                   double *const y) {
  Rzaxpy(&n, d, x, &fmr::N_ONE, y, &fmr::N_ONE);
}

// Blas 2 for general dense matrices

// y = d Ax
inline void gemv(const unsigned m, const unsigned n, double d, double *A,
                 double *x, double *y) {
  Rdgemv(fmr::JOB_STR, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::D_ZERO, y,
         &fmr::N_ONE);
}
inline void gemv(const unsigned m, const unsigned n, float d, float *A,
                 float *x, float *y) {
  Rsgemv(fmr::JOB_STR, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::S_ZERO, y,
         &fmr::N_ONE);
}
inline void c_gemv(const unsigned m, const unsigned n, float *d, float *A,
                   float *x, float *y) {
  Rcgemv(fmr::JOB_STR, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::C_ZERO, y,
         &fmr::N_ONE);
}
inline void c_gemv(const unsigned m, const unsigned n, double *d, double *A,
                   double *x, double *y) {
  Rzgemv(fmr::JOB_STR, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::Z_ZERO, y,
         &fmr::N_ONE);
}

// y += d Ax
inline void gemva(const unsigned m, const unsigned n, double d, double *A,
                  double *x, double *y) {
  Rdgemv(fmr::JOB_STR, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::D_ONE, y,
         &fmr::N_ONE);
}
inline void gemva(const unsigned m, const unsigned n, float d, float *A,
                  float *x, float *y) {
  Rsgemv(fmr::JOB_STR, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::S_ONE, y,
         &fmr::N_ONE);
}
inline void c_gemva(const unsigned m, const unsigned n, const float *d,
                    const float *A, const float *x, float *y) {
  Rcgemv(fmr::JOB_STR, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::C_ONE, y,
         &fmr::N_ONE);
}
inline void c_gemva(const unsigned m, const unsigned n, const double *d,
                    const double *A, const double *x, double *y) {
  Rzgemv(fmr::JOB_STR, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::Z_ONE, y,
         &fmr::N_ONE);
}

// y = d A^T x
inline void gemtv(const unsigned m, const unsigned n, double d, double *A,
                  double *x, double *y) {
  Rdgemv(fmr::JOB_STR + 1, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::D_ZERO, y,
         &fmr::N_ONE);
}
inline void gemtv(const unsigned m, const unsigned n, float d, float *A,
                  float *x, float *y) {
  Rsgemv(fmr::JOB_STR + 1, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::S_ZERO, y,
         &fmr::N_ONE);
}
inline void c_gemtv(const unsigned m, const unsigned n, float *d, float *A,
                    float *x, float *y) {
  Rcgemv(fmr::JOB_STR + 1, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::C_ZERO, y,
         &fmr::N_ONE);
}
inline void c_gemtv(const unsigned m, const unsigned n, double *d, double *A,
                    double *x, double *y) {
  Rzgemv(fmr::JOB_STR + 1, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::Z_ZERO, y,
         &fmr::N_ONE);
}
inline void c_gemhv(const unsigned m, const unsigned n, float *d, float *A,
                    float *x, float *y) {
  Rcgemv(fmr::JOB_STR + 7, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::C_ZERO, y,
         &fmr::N_ONE);
} // hermitian transposed
inline void c_gemhv(const unsigned m, const unsigned n, double *d, double *A,
                    double *x, double *y) {
  Rzgemv(fmr::JOB_STR + 7, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::Z_ZERO, y,
         &fmr::N_ONE);
} // hermitian transposed

// y += d A^T x
inline void gemtva(const unsigned m, const unsigned n, double d, double *A,
                   double *x, double *y) {
  Rdgemv(fmr::JOB_STR + 1, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::D_ONE, y,
         &fmr::N_ONE);
}
inline void gemtva(const unsigned m, const unsigned n, float d, float *A,
                   float *x, float *y) {
  Rsgemv(fmr::JOB_STR + 1, &m, &n, &d, A, &m, x, &fmr::N_ONE, &fmr::S_ONE, y,
         &fmr::N_ONE);
}
inline void c_gemtva(const unsigned m, const unsigned n, float *d, float *A,
                     float *x, float *y) {
  Rcgemv(fmr::JOB_STR + 1, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::C_ONE, y,
         &fmr::N_ONE);
}
inline void c_gemtva(const unsigned m, const unsigned n, double *d, double *A,
                     double *x, double *y) {
  Rzgemv(fmr::JOB_STR + 1, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::Z_ONE, y,
         &fmr::N_ONE);
}
inline void c_gemhva(const unsigned m, const unsigned n, float *d, float *A,
                     float *x, float *y) {
  Rcgemv(fmr::JOB_STR + 7, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::C_ONE, y,
         &fmr::N_ONE);
} // hermitian transposed
inline void c_gemhva(const unsigned m, const unsigned n, double *d, double *A,
                     double *x, double *y) {
  Rzgemv(fmr::JOB_STR + 7, &m, &n, d, A, &m, x, &fmr::N_ONE, fmr::Z_ONE, y,
         &fmr::N_ONE);
} // hermitian transposed

// Blas 2 for symmetric dense matrices

// y = d Ax, A is a m x m symmetric matrix
inline void symv(const unsigned m, double d, double *A, double *x, double *y) {
  Rdsymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, &d, A, &m, x, &fmr::N_ONE,
         &fmr::D_ZERO, y, &fmr::N_ONE);
}
inline void symv(const unsigned m, float d, float *A, float *x, float *y) {
  Rssymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, &d, A, &m, x, &fmr::N_ONE,
         &fmr::S_ZERO, y, &fmr::N_ONE);
}
inline void c_symv(const unsigned m, float *d, float *A, float *x, float *y) {
  Rcsymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, d, A, &m, x, &fmr::N_ONE,
         fmr::C_ZERO, y, &fmr::N_ONE);
}
inline void c_symv(const unsigned m, double *d, double *A, double *x,
                   double *y) {
  Rzsymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, d, A, &m, x, &fmr::N_ONE,
         fmr::Z_ZERO, y, &fmr::N_ONE);
}

// y += d Ax, A is a m x m symmetric matrix
inline void symva(const unsigned m, double d, double *A, double *x, double *y) {
  Rdsymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, &d, A, &m, x, &fmr::N_ONE,
         &fmr::D_ONE, y, &fmr::N_ONE);
}
inline void symva(const unsigned m, float d, float *A, float *x, float *y) {
  Rssymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, &d, A, &m, x, &fmr::N_ONE,
         &fmr::S_ONE, y, &fmr::N_ONE);
}
inline void c_symva(const unsigned m, const float *d, const float *A,
                    const float *x, float *y) {
  Rcsymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, d, A, &m, x, &fmr::N_ONE, fmr::C_ONE,
         y, &fmr::N_ONE);
}
inline void c_symva(const unsigned m, const double *d, const double *A,
                    const double *x, double *y) {
  Rzsymv(fmr::JOB_STR + 5 /*U=Upper*/, &m, d, A, &m, x, &fmr::N_ONE, fmr::Z_ONE,
         y, &fmr::N_ONE);
}

// Blas 3 for general dense matrices

// C <- alpha A B + beta C, A is m x k, B is k x n
inline void gemm(unsigned m, unsigned k, unsigned n,
                 double alpha, double *A, unsigned ldA,
                               double *B, unsigned ldB,
                 double beta,  double *C, unsigned ldC) {
  Rdgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &k, &alpha, A, &ldA, B, &ldB, &beta, C, &ldC);
}
inline void gemm(unsigned m, unsigned k, unsigned n,
                 float alpha, float *A, unsigned ldA,
                              float *B, unsigned ldB,
                 float beta,  float *C, unsigned ldC) {
  Rsgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &k, &alpha, A, &ldA, B, &ldB, &beta, C, &ldC);
}
inline void c_gemm(const unsigned m, const unsigned p, const unsigned n,
                   const float *d, float *A, const unsigned ldA, float *B,
                   const unsigned ldB, float *C, const unsigned ldC) {
  Rcgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::C_ZERO, C, &ldC);
}
inline void c_gemm(const unsigned m, const unsigned p, const unsigned n,
                   const double *d, double *A, const unsigned ldA, double *B,
                   const unsigned ldB, double *C, const unsigned ldC) {
  Rzgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::Z_ZERO, C, &ldC);
}

inline double *double_gemm_alloc(unsigned m, unsigned p, unsigned n) {
  return Rdgemm_alloc(fmr::JOB_STR + 10 /*A*/, &m, &n, &p);
}
inline float *single_gemm_alloc(unsigned m, unsigned p, unsigned n) {
  return Rsgemm_alloc(fmr::JOB_STR + 10 /*A*/, &m, &n, &p);
}
inline float *c_single_gemm_alloc(const unsigned m, const unsigned p,
                                  const unsigned n) {
  return Rcgemm_alloc(fmr::JOB_STR + 10 /*A*/, &m, &n, &p);
}
inline double *c_double_gemm_alloc(const unsigned m, const unsigned p,
                                   const unsigned n) {
  return Rzgemm_alloc(fmr::JOB_STR + 10 /*A*/, &m, &n, &p);
}

inline void gemm_pack(unsigned m, unsigned p, unsigned n, double d,
                      const double *A, unsigned ldA, double *Ap) {
  Rdgemm_pack(fmr::JOB_STR + 10 /*A*/, fmr::JOB_STR /*N*/, &m, &n, &p, &d, A,
              &ldA, Ap);
}
inline void gemm_pack(unsigned m, unsigned p, unsigned n, float d,
                      const float *A, unsigned ldA, float *Ap) {
  Rsgemm_pack(fmr::JOB_STR + 10 /*A*/, fmr::JOB_STR /*N*/, &m, &n, &p, &d, A,
              &ldA, Ap);
}
inline void c_gemm_pack(const unsigned m, const unsigned p, const unsigned n,
                        const float *d, const float *A, const unsigned ldA,
                        float *Ap) {
  Rcgemm_pack(fmr::JOB_STR + 10 /*A*/, fmr::JOB_STR /*N*/, &m, &n, &p, d, A,
              &ldA, Ap);
}
inline void c_gemm_pack(const unsigned m, const unsigned p, const unsigned n,
                        const double *d, const double *A, const unsigned ldA,
                        double *Ap) {
  Rzgemm_pack(fmr::JOB_STR + 10 /*A*/, fmr::JOB_STR /*N*/, &m, &n, &p, d, A,
              &ldA, Ap);
}

inline void gemm_compute(unsigned m, unsigned p, unsigned n, double *A,
                         unsigned ldA, double *B, unsigned ldB, double *C,
                         unsigned ldC) {
  Rdgemm_compute(fmr::JOB_STR + 9 /*P*/, fmr::JOB_STR /*N*/, &m, &n, &p, A,
                 &ldA, B, &ldB, &fmr::D_ZERO, C, &ldC);
}
inline void gemm_compute(unsigned m, unsigned p, unsigned n, float *A,
                         unsigned ldA, float *B, unsigned ldB, float *C,
                         unsigned ldC) {
  Rsgemm_compute(fmr::JOB_STR + 9 /*P*/, fmr::JOB_STR /*N*/, &m, &n, &p, A,
                 &ldA, B, &ldB, &fmr::S_ZERO, C, &ldC);
}
inline void c_gemm_compute(const unsigned m, const unsigned p, const unsigned n,
                           float *A, const unsigned ldA, float *B,
                           const unsigned ldB, float *C, const unsigned ldC) {
  Rcgemm_compute(fmr::JOB_STR + 9 /*P*/, fmr::JOB_STR /*N*/, &m, &n, &p, A,
                 &ldA, B, &ldB, fmr::C_ZERO, C, &ldC);
}
inline void c_gemm_compute(const unsigned m, const unsigned p, const unsigned n,
                           double *A, const unsigned ldA, double *B,
                           const unsigned ldB, double *C, const unsigned ldC) {
  Rzgemm_compute(fmr::JOB_STR + 9 /*P*/, fmr::JOB_STR /*N*/, &m, &n, &p, A,
                 &ldA, B, &ldB, fmr::Z_ZERO, C, &ldC);
}

inline void gemm_free(double *Ap) { Rdgemm_free(Ap); }
inline void gemm_free(float *Ap) { Rsgemm_free(Ap); }
inline void c_gemm_free(float *Ap) { Rcgemm_free(Ap); }
inline void c_gemm_free(double *Ap) { Rzgemm_free(Ap); }

// C += d A B, A is m x p, B is p x n
inline void gemma(unsigned m, unsigned p, unsigned n, double d, double *A,
                  unsigned ldA, double *B, unsigned ldB, double *C,
                  unsigned ldC) {
  Rdgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &p, &d, A, &ldA, B, &ldB,
         &fmr::D_ONE, C, &ldC);
}
inline void gemma(unsigned m, unsigned p, unsigned n, float d, float *A,
                  unsigned ldA, float *B, unsigned ldB, float *C,
                  unsigned ldC) {
  Rsgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &p, &d, A, &ldA, B, &ldB,
         &fmr::S_ONE, C, &ldC);
}
inline void c_gemma(unsigned m, unsigned p, unsigned n, float *d, float *A,
                    unsigned ldA, float *B, unsigned ldB, float *C,
                    unsigned ldC) {
  Rcgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::C_ONE, C, &ldC);
}
inline void c_gemma(unsigned m, unsigned p, unsigned n, double *d, double *A,
                    unsigned ldA, double *B, unsigned ldB, double *C,
                    unsigned ldC) {
  Rzgemm(fmr::JOB_STR, fmr::JOB_STR, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::Z_ONE, C, &ldC);
}

// C <- alpha A^T B + beta C, A is m x k, B is m x n
inline void gemtm(unsigned m, unsigned k, unsigned n,
                  double alpha, double *A, unsigned ldA,
                                double *B, unsigned ldB,
                  double beta,  double *C, unsigned ldC) {
  Rdgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &k, &n, &m, &alpha, A, &ldA, B, &ldB,
         &beta, C, &ldC);
}
inline void gemtm(unsigned m, unsigned k, unsigned n,
                  float alpha, float *A, unsigned ldA,
                               float *B, unsigned ldB,
                  float beta,  float *C, unsigned ldC) {
  Rsgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &k, &n, &m, &alpha, A, &ldA, B, &ldB,
         &beta, C, &ldC);
}
inline void c_gemtm(unsigned m, unsigned p, unsigned n, float *d, float *A,
                    unsigned ldA, float *B, unsigned ldB, float *C,
                    unsigned ldC) {
  Rcgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::C_ZERO, C, &ldC);
}
inline void c_gemtm(unsigned m, unsigned p, unsigned n, double *d, double *A,
                    unsigned ldA, double *B, unsigned ldB, double *C,
                    unsigned ldC) {
  Rzgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::Z_ZERO, C, &ldC);
}
inline void c_gemhm(unsigned m, unsigned p, unsigned n, float *d, float *A,
                    unsigned ldA, float *B, unsigned ldB, float *C,
                    unsigned ldC) {
  Rcgemm(fmr::JOB_STR + 7, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::C_ZERO, C, &ldC);
}
inline void c_gemhm(unsigned m, unsigned p, unsigned n, double *d, double *A,
                    unsigned ldA, double *B, unsigned ldB, double *C,
                    unsigned ldC) {
  Rzgemm(fmr::JOB_STR + 7, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::Z_ZERO, C, &ldC);
}

// C += d A^T B, A is m x p, B is m x n
inline void gemtma(unsigned m, unsigned p, unsigned n, double d, double *A,
                   unsigned ldA, double *B, unsigned ldB, double *C,
                   unsigned ldC) {
  Rdgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &p, &n, &m, &d, A, &ldA, B, &ldB,
         &fmr::D_ONE, C, &ldC);
}
inline void gemtma(unsigned m, unsigned p, unsigned n, float d, float *A,
                   unsigned ldA, float *B, unsigned ldB, float *C,
                   unsigned ldC) {
  Rsgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &p, &n, &m, &d, A, &ldA, B, &ldB,
         &fmr::S_ONE, C, &ldC);
}
inline void c_gemtma(unsigned m, unsigned p, unsigned n, float *d, float *A,
                     unsigned ldA, float *B, unsigned ldB, float *C,
                     unsigned ldC) {
  Rcgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::C_ONE, C, &ldC);
}
inline void c_gemtma(unsigned m, unsigned p, unsigned n, double *d, double *A,
                     unsigned ldA, double *B, unsigned ldB, double *C,
                     unsigned ldC) {
  Rzgemm(fmr::JOB_STR + 1, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::Z_ONE, C, &ldC);
}
inline void c_gemhma(unsigned m, unsigned p, unsigned n, float *d, float *A,
                     unsigned ldA, float *B, unsigned ldB, float *C,
                     unsigned ldC) {
  Rcgemm(fmr::JOB_STR + 7, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::C_ONE, C, &ldC);
}
inline void c_gemhma(unsigned m, unsigned p, unsigned n, double *d, double *A,
                     unsigned ldA, double *B, unsigned ldB, double *C,
                     unsigned ldC) {
  Rzgemm(fmr::JOB_STR + 7, fmr::JOB_STR, &p, &n, &m, d, A, &ldA, B, &ldB,
         fmr::Z_ONE, C, &ldC);
}

// C <- alpha A B^T + beta C, A is m x k, B is n x k
inline void gemmt(unsigned m, unsigned k, unsigned n,
                  double alpha, double *A, unsigned ldA,
                                double *B, unsigned ldB,
                  double beta,  double *C, unsigned ldC) {
  Rdgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &k, &alpha, A, &ldA, B, &ldB,
         &beta, C, &ldC);
}
inline void gemmt(unsigned m, unsigned k, unsigned n,
                  float alpha, float *A, unsigned ldA,
                               float *B, unsigned ldB,
                  float beta,  float *C, unsigned ldC) {
  Rsgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &k, &alpha, A, &ldA, B, &ldB,
         &beta, C, &ldC);
}
inline void c_gemmt(unsigned m, unsigned p, unsigned n, float d, float *A,
                    unsigned ldA, float *B, unsigned ldB, float *C,
                    unsigned ldC) {
  Rcgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &p, &d, A, &ldA, B, &ldB,
         fmr::C_ZERO, C, &ldC);
}
inline void c_gemmt(unsigned m, unsigned p, unsigned n, double d, double *A,
                    unsigned ldA, double *B, unsigned ldB, double *C,
                    unsigned ldC) {
  Rzgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &p, &d, A, &ldA, B, &ldB,
         fmr::Z_ZERO, C, &ldC);
}
inline void c_gemmh(unsigned m, unsigned p, unsigned n, float d, float *A,
                    unsigned ldA, float *B, unsigned ldB, float *C,
                    unsigned ldC) {
  Rcgemm(fmr::JOB_STR, fmr::JOB_STR + 7, &m, &n, &p, &d, A, &ldA, B, &ldB,
         fmr::C_ZERO, C, &ldC);
}
inline void c_gemmh(unsigned m, unsigned p, unsigned n, double d, double *A,
                    unsigned ldA, double *B, unsigned ldB, double *C,
                    unsigned ldC) {
  Rzgemm(fmr::JOB_STR, fmr::JOB_STR + 7, &m, &n, &p, &d, A, &ldA, B, &ldB,
         fmr::Z_ZERO, C, &ldC);
}

// C <- alpha A^T B^T + beta C, A is m x k, B is n x k
inline void gemmtt(unsigned m, unsigned k, unsigned n,
                   double alpha, double *A, unsigned ldA,
                                 double *B, unsigned ldB,
                   double beta,  double *C, unsigned ldC) {
  Rdgemm(fmr::JOB_STR + 1, fmr::JOB_STR + 1, &m, &n, &k, &alpha, A, &ldA, B, &ldB,
         &beta, C, &ldC);
}
inline void gemmtt(unsigned m, unsigned k, unsigned n,
                   float alpha, float *A, unsigned ldA,
                                float *B, unsigned ldB,
                   float beta,  float *C, unsigned ldC) {
  Rsgemm(fmr::JOB_STR + 1, fmr::JOB_STR + 1, &m, &n, &k, &alpha, A, &ldA, B, &ldB,
         &beta, C, &ldC);
}
inline void c_gemmtt(unsigned m, unsigned p, unsigned n, float d, float *A,
                    unsigned ldA, float *B, unsigned ldB, float *C,
                    unsigned ldC) {
  Rcgemm(fmr::JOB_STR + 1, fmr::JOB_STR + 1, &m, &n, &p, &d, A, &ldA, B, &ldB,
         fmr::C_ZERO, C, &ldC);
}
inline void c_gemmtt(unsigned m, unsigned p, unsigned n, double d, double *A,
                    unsigned ldA, double *B, unsigned ldB, double *C,
                    unsigned ldC) {
  Rzgemm(fmr::JOB_STR + 1, fmr::JOB_STR + 1, &m, &n, &p, &d, A, &ldA, B, &ldB,
         fmr::Z_ZERO, C, &ldC);
}

// C += d A B^T, A is m x p, B is n x p
inline void gemmta(unsigned m, unsigned p, unsigned n, double d, double *A,
                   unsigned ldA, double *B, unsigned ldB, double *C,
                   unsigned ldC) {
  Rdgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &p, &d, A, &ldA, B, &ldB,
         &fmr::D_ONE, C, &ldC);
}
inline void gemmta(unsigned m, unsigned p, unsigned n, float d, float *A,
                   unsigned ldA, float *B, unsigned ldB, float *C,
                   unsigned ldC) {
  Rsgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &p, &d, A, &ldA, B, &ldB,
         &fmr::S_ONE, C, &ldC);
}
inline void c_gemmta(unsigned m, unsigned p, unsigned n, float *d, float *A,
                     unsigned ldA, float *B, unsigned ldB, float *C,
                     unsigned ldC) {
  Rcgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::C_ONE, C, &ldC);
}
inline void c_gemmta(unsigned m, unsigned p, unsigned n, double *d, double *A,
                     unsigned ldA, double *B, unsigned ldB, double *C,
                     unsigned ldC) {
  Rzgemm(fmr::JOB_STR, fmr::JOB_STR + 1, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::Z_ONE, C, &ldC);
}
inline void c_gemmha(unsigned m, unsigned p, unsigned n, float *d, float *A,
                     unsigned ldA, float *B, unsigned ldB, float *C,
                     unsigned ldC) {
  Rcgemm(fmr::JOB_STR, fmr::JOB_STR + 7, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::C_ONE, C, &ldC);
}
inline void c_gemmha(unsigned m, unsigned p, unsigned n, double *d, double *A,
                     unsigned ldA, double *B, unsigned ldB, double *C,
                     unsigned ldC) {
  Rzgemm(fmr::JOB_STR, fmr::JOB_STR + 7, &m, &n, &p, d, A, &ldA, B, &ldB,
         fmr::Z_ONE, C, &ldC);
}

// Blas 3 for symmetric dense matrices

// C = d A B, A is m x m symmetric matrix, B and C are m x n matrices
inline void symm(unsigned m, unsigned n, double d, double *A, unsigned ldA,
                 double *B, unsigned ldB, double *C, unsigned ldC) {
  Rdsymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, &d,
         A, &ldA, B, &ldB, &fmr::D_ZERO, C, &ldC);
}
inline void symm(unsigned m, unsigned n, float d, float *A, unsigned ldA,
                 float *B, unsigned ldB, float *C, unsigned ldC) {
  Rssymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, &d,
         A, &ldA, B, &ldB, &fmr::S_ZERO, C, &ldC);
}
inline void c_symm(const unsigned m, const unsigned n, const float *d, float *A,
                   const unsigned ldA, float *B, const unsigned ldB, float *C,
                   const unsigned ldC) {
  Rcsymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, d,
         A, &ldA, B, &ldB, fmr::C_ZERO, C, &ldC);
}
inline void c_symm(const unsigned m, const unsigned n, const double *d,
                   double *A, const unsigned ldA, double *B, const unsigned ldB,
                   double *C, const unsigned ldC) {
  Rzsymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, d,
         A, &ldA, B, &ldB, fmr::Z_ZERO, C, &ldC);
}

// C += d A B, A is m x m symmetric matrix, B and C are m x n matrices
inline void symma(unsigned m, unsigned n, double d, double *A, unsigned ldA,
                  double *B, unsigned ldB, double *C, unsigned ldC) {
  Rdsymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, &d,
         A, &ldA, B, &ldB, &fmr::D_ONE, C, &ldC);
}
inline void symma(unsigned m, unsigned n, float d, float *A, unsigned ldA,
                  float *B, unsigned ldB, float *C, unsigned ldC) {
  Rssymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, &d,
         A, &ldA, B, &ldB, &fmr::S_ONE, C, &ldC);
}
inline void c_symma(unsigned m, unsigned n, float *d, float *A, unsigned ldA,
                    float *B, unsigned ldB, float *C, unsigned ldC) {
  Rcsymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, d,
         A, &ldA, B, &ldB, fmr::C_ONE, C, &ldC);
}
inline void c_symma(unsigned m, unsigned n, double *d, double *A, unsigned ldA,
                    double *B, unsigned ldB, double *C, unsigned ldC) {
  Rzsymm(fmr::JOB_STR + 6 /*L=Left*/, fmr::JOB_STR + 5 /*U=Upper*/, &m, &n, d,
         A, &ldA, B, &ldB, fmr::Z_ONE, C, &ldC);
}

//
// eigen value decomposition (JOB_STR = "NTOSVULCRPA")
//
// BEWARE! The recommended lwork >= max(1,4n) does not  seem to be sufficient
// (error at runtime)!
//
inline int geev(unsigned n, double *A, double *reE, double *imE, double *VL,
                unsigned ldVL, double *VR, unsigned ldVR, int nwk, double *wk) {
  int INF;
  Rdgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 4 /*V-N or V*/,
         &n, A, &n, reE, imE, VL /*VL*/, &ldVL, VR, &ldVR, wk, &nwk, &INF);
  return INF;
}
inline int geev(unsigned n, float *A, float *reE, float *imE, float *VL,
                unsigned ldVL, float *VR, unsigned ldVR, int nwk, float *wk) {
  int INF;
  Rsgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 4 /*V-N or V*/,
         &n, A, &n, reE, imE, VL /*VL*/, &ldVL, VR, &ldVR, wk, &nwk, &INF);
  return INF;
}

/*
 * geev_opt: First query optimal workspace then allocate it and run geev
 */
inline int geev_opt(unsigned n, double *A, double *reE, double *imE, double *VL,
                    unsigned ldVL, double *VR, unsigned ldVR) {
  int INF;

  /* Query and allocate the optimal workspace */
  double wkopt;
  int nwk = -1;
  Rdgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 4 /*V-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, &wkopt, &nwk, &INF);
  //
  nwk = (int)wkopt;
  std::printf(" Optimal workspace size = %d = %.2f x size\n", nwk,
              double(nwk) / double(n));
  double *wk = (double *)malloc(nwk * sizeof(double));
  /* Solve eigenproblem */
  Rdgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 4 /*V-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, wk, &nwk, &INF);

  return INF;
}
inline int geev_opt(unsigned n, float *A, float *reE, float *imE, float *VL,
                    unsigned ldVL, float *VR, unsigned ldVR) {
  int INF;

  /* Query and allocate the optimal workspace */
  float wkopt;
  int nwk = -1;
  Rsgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 4 /*V-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, &wkopt, &nwk, &INF);
  //
  nwk = (int)wkopt;
  std::printf(" Optimal workspace size = %d = %.2f x size\n", nwk,
              float(nwk) / float(n));
  float *wk = (float *)malloc(nwk * sizeof(float));
  /* Solve eigenproblem */
  Rsgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 4 /*V-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, wk, &nwk, &INF);

  return INF;
}

/*
 * geev_opt: First query optimal workspace then allocate it and run geev
 */
inline int geev_opt(unsigned n, double *A, double *reE, double *imE, double *VL,
                    unsigned ldVL) {
  int INF;

  double *VR = NULL;
  unsigned ldVR = 0;

  /* Query and allocate the optimal workspace */
  double wkopt;
  int nwk = -1;
  Rdgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, &wkopt, &nwk, &INF);
  //
  nwk = (int)wkopt;
  std::printf(" Optimal workspace size = %d = %.2f x size\n", nwk,
              double(nwk) / double(n));
  double *wk = (double *)malloc(nwk * sizeof(double));
  /* Solve eigenproblem */
  Rdgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, wk, &nwk, &INF);

  return INF;
}
inline int geev_opt(unsigned n, float *A, float *reE, float *imE, float *VL,
                    unsigned ldVL) {
  int INF;

  float *VR = NULL;
  unsigned ldVR = 0;

  /* Query and allocate the optimal workspace */
  float wkopt;
  int nwk = -1;
  Rsgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, &wkopt, &nwk, &INF);
  //
  nwk = (int)wkopt;
  std::printf(" Optimal workspace size = %d = %.2f x size\n", nwk,
              float(nwk) / float(n));
  float *wk = (float *)malloc(nwk * sizeof(float));
  /* Solve eigenproblem */
  Rsgeev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, wk, &nwk, &INF);

  return INF;
}

/*
 * geev_opt: First query optimal workspace then allocate it and run geev
 */
inline int geev_opt(unsigned n, double *A, double *reE, double *imE) {
  int INF;

  double *VL = NULL;
  unsigned ldVL = 0;
  double *VR = NULL;
  unsigned ldVR = 0;

  /* Query and allocate the optimal workspace */
  double wkopt;
  int nwk = -1;
  Rdgeev(fmr::JOB_STR + 0 /*N-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, &wkopt, &nwk, &INF);
  //
  nwk = (int)wkopt;
  std::printf(" Optimal workspace size = %d = %.2f x size\n", nwk,
              double(nwk) / double(n));
  double *wk = (double *)malloc(nwk * sizeof(double));
  /* Solve eigenproblem */
  Rdgeev(fmr::JOB_STR + 0 /*N-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, wk, &nwk, &INF);

  return INF;
}
inline int geev_opt(unsigned n, float *A, float *reE, float *imE) {
  int INF;

  float *VL = NULL;
  unsigned ldVL = 0;
  float *VR = NULL;
  unsigned ldVR = 0;

  /* Query and allocate the optimal workspace */
  float wkopt;
  int nwk = -1;
  Rsgeev(fmr::JOB_STR + 0 /*N-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, &wkopt, &nwk, &INF);
  //
  nwk = (int)wkopt;
  std::printf(" Optimal workspace size = %d = %.2f x size\n", nwk,
              float(nwk) / float(n));
  float *wk = (float *)malloc(nwk * sizeof(float));
  /* Solve eigenproblem */
  Rsgeev(fmr::JOB_STR + 0 /*N-N(+0) or V(+4)*/, fmr::JOB_STR + 0 /*N-N or V*/,
         &n, A, &n, reE, imE, VL, &ldVL, VR, &ldVR, wk, &nwk, &INF);

  return INF;
}

/*
 *
 * SYEV AND SYEV_OPT NOT WORKING YET!!!!
 *
 */
inline int syev(unsigned n, double *A, double *E, int nwk, double *wk) {
  int INF;
  Rdsyev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/,
         fmr::JOB_STR + 6 /*L-U(+5) or L(+6)*/, &n, A, &n, E, wk, &nwk, &INF);
  return INF;
}

inline int syev(unsigned n, float *A, float *E, int nwk, float *wk) {
  int INF;
  Rssyev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/,
         fmr::JOB_STR + 6 /*L-U(+5) or L(+6)*/, &n, A, &n, E, wk, &nwk, &INF);
  return INF;
}

inline int syev_opt(unsigned n, double *A, double *E) {
  int INF;

  /* Query and allocate the optimal workspace */
  int nwk = -1;
  double wkopt;
  Rdsyev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/,
         fmr::JOB_STR + 6 /*L-U(+5) or L(+6)*/, &n, A, &n, E, &wkopt, &nwk,
         &INF);
  /* Solve eigenproblem */
  nwk = (int)wkopt;
  double *wk = (double *)malloc(nwk * sizeof(double));
  //
  std::printf(" Optimal workspace size = %d = %.2f x size\n", nwk,
              double(nwk) / double(n));
  //
  Rdsyev(fmr::JOB_STR + 4 /*V-N(+0) or V(+4)*/,
         fmr::JOB_STR + 6 /*L-U(+5) or L(+6)*/, &n, A, &n, E, wk, &nwk, &INF);
  /* Check for convergence */
  if (INF > 0) {
    std::printf("The algorithm failed to compute eigenvalues.\n");
    std::exit(EXIT_FAILURE);
  }
  if (INF < 0) {
    std::printf("The %d-th parameter had an illegal value.\n", INF);
    std::exit(EXIT_FAILURE);
  }

  return INF;
}

// singular value decomposition
//
inline int gesvd(unsigned m, unsigned n, double *A, double *S, double *VT,
                 unsigned ldVT, int nwk, double *wk) {
  int INF;
  Rdgesvd(fmr::JOB_STR + 2, fmr::JOB_STR + 3, &m, &n, A, &m, S, A, &m, VT,
          &ldVT, wk, &nwk, &INF);
  return INF;
}
//
//    A = U * SIGMA * conjugate-transpose(V)
// fmr::JOB_STR+2 = 'O':  the first min(m,n) columns of U (the left singular
// vectors) are overwritten on the array A;
inline int c_gesvd(unsigned m, unsigned n, double *A, double *S, double *VT,
                   unsigned ldVT, int &nwk, double *wk, double *rwk) {
  int INF;
  Rzgesvd(fmr::JOB_STR + 2, fmr::JOB_STR + 3, &m, &n, A, &m, S, A, &m, VT,
          &ldVT, wk, &nwk, rwk, &INF);
  return INF;
}

inline int gesvd(unsigned m, unsigned n, float *A, float *S, float *VT,
                 unsigned ldVT, int nwk, float *wk) {
  int INF;
  Rsgesvd(fmr::JOB_STR + 2, fmr::JOB_STR + 3, &m, &n, A, &m, S, A, &m, VT,
          &ldVT, wk, &nwk, &INF);
  return INF;
}

// singular value decomposition (SO)
inline int gesvdSO(unsigned m, unsigned n, double *A, double *S, double *U,
                   unsigned ldU, int nwk, double *wk) {
  int INF;
  Rdgesvd(fmr::JOB_STR + 3, fmr::JOB_STR + 2, &m, &n, A, &m, S, U, &m, A, &ldU,
          wk, &nwk, &INF);
  return INF;
}
inline int gesvdSO(unsigned m, unsigned n, float *A, float *S, float *U,
                   unsigned ldU, int nwk, float *wk) {
  int INF;
  Rsgesvd(fmr::JOB_STR + 3, fmr::JOB_STR + 2, &m, &n, A, &m, S, U, &m, A, &ldU,
          wk, &nwk, &INF);
  return INF;
}

// singular value decomposition (AA)
inline int gesvdAA(unsigned m, unsigned n, double *A, double *S, double *U,
                   unsigned ldU, int nwk, double *wk) {
  int INF;
  Rdgesvd("A", "A", &m, &n, A, &m, S, U, &m, A, &ldU, wk, &nwk, &INF);
  return INF;
}
inline int gesvdAA(unsigned m, unsigned n, float *A, float *S, float *U,
                   unsigned ldU, int nwk, float *wk) {
  int INF;
  Rsgesvd("A", "A", &m, &n, A, &m, S, U, &m, A, &ldU, wk, &nwk, &INF);
  return INF;
}

// Scalar product v1'*v2
inline double scpr(const unsigned n, const double *const v1,
                   const double *const v2) {
  return Rddot(&n, v1, &fmr::N_ONE, v2, &fmr::N_ONE);
}
inline float scpr(const unsigned n, const float *const v1,
                  const float *const v2) {
  return Rsdot(&n, v1, &fmr::N_ONE, v2, &fmr::N_ONE);
}
inline double dot(const unsigned n, const double *const v1,
                  const double *const v2) {
  return Rddot(&n, v1, &fmr::N_ONE, v2, &fmr::N_ONE);
}
inline float dot(const unsigned n, const float *const v1,
                 const float *const v2) {
  return Rsdot(&n, v1, &fmr::N_ONE, v2, &fmr::N_ONE);
}
// QR factorisation
inline int geqrf(const unsigned m, const unsigned n, double *A, double *tau,
                 unsigned nwk, double *wk) {
  int INF;
  Rdgeqrf(&m, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
inline int geqrf(const unsigned m, const unsigned n, float *A, float *tau,
                 unsigned nwk, float *wk) {
  int INF;
  Rsgeqrf(&m, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
// QR factorisation with column pivoting
inline int geqp3(const unsigned m, const unsigned n, double *A, unsigned *jpiv,
                 double *tau, unsigned nwk, double *wk) {
  int INF;
  Rdgeqp3(&m, &n, A, &m, jpiv, tau, wk, &nwk, &INF);
  return INF;
}
inline int geqp3(const unsigned m, const unsigned n, float *A, unsigned *jpiv,
                 float *tau, unsigned nwk, float *wk) {
  int INF;
  Rsgeqp3(&m, &n, A, &m, jpiv, tau, wk, &nwk, &INF);
  return INF;
}
inline int c_geqrf(const unsigned m, const unsigned n, float *A, float *tau,
                   unsigned nwk, float *wk) {
  int INF;
  Rcgeqrf(&m, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}

inline int c_geqrf(const unsigned m, const unsigned n, double *A, double *tau,
                   unsigned nwk, double *wk) {
  int INF;
  Rzgeqrf(&m, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
inline int c_geqp3(const unsigned m, const unsigned n, float *A, unsigned *jpiv,
                   float *tau, unsigned nwk, float *wk) {
  int INF;
  Rcgeqp3(&m, &n, A, &m, jpiv, tau, wk, &nwk, &INF);
  return INF;
}

inline int c_geqp3(const unsigned m, const unsigned n, double *A,
                   unsigned *jpiv, double *tau, unsigned nwk, double *wk) {
  int INF;
  Rzgeqp3(&m, &n, A, &m, jpiv, tau, wk, &nwk, &INF);
  return INF;
}

// return full of Q-Matrix (QR factorization) in A
inline int orgqr_full(const unsigned m, const unsigned n, double *A,
                      double *tau, unsigned nwk, double *wk) {
  int INF;
  Rdorgqr(&m, &m, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
inline int orgqr_full(const unsigned m, const unsigned n, float *A, float *tau,
                      unsigned nwk, float *wk) {
  int INF;
  Rsorgqr(&m, &m, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
// return the leading n columns of Q-Matrix (QR factorization) in A
inline int orgqr(const unsigned m, const unsigned n, double *A, double *tau,
                 unsigned nwk, double *wk) {
  int INF;
  Rdorgqr(&m, &n, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
inline int orgqr(const unsigned m, const unsigned n, float *A, float *tau,
                 unsigned nwk, float *wk) {
  int INF;
  Rsorgqr(&m, &n, &n, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
// Same functions as above, but with an extra parameter
inline int orgqr(const unsigned m, const unsigned n, const unsigned k,
                 double *A, double *tau, unsigned nwk, double *wk) {
  int INF;
  Rdorgqr(&m, &n, &k, A, &m, tau, wk, &nwk, &INF);
  return INF;
}
inline int orgqr(const unsigned m, const unsigned n, const unsigned k, float *A,
                 float *tau, unsigned nwk, float *wk) {
  int INF;
  Rsorgqr(&m, &n, &k, A, &m, tau, wk, &nwk, &INF);
  return INF;
}

// apply Q-Matrix (from QR factorization) to C
// LEFT: Q(^T)C
inline int left_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                      const double *A, double *tau, double *C, unsigned nwk,
                      double *wk) {
  int INF;
  if (n < m) {
    Rdormqr("L", TRANS, &m, &n, &n, A, &m, tau, C, &m, wk, &nwk, &INF);
  } else {
    Rdormqr("L", TRANS, &m, &n, &n, A, &m, tau, C, &m, wk, &nwk, &INF);
  }
  return INF;
}
inline int left_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                      const unsigned k, const double *A, double *tau, double *C,
                      unsigned nwk, double *wk) {
  int INF;
  Rdormqr("L", TRANS, &m, &n, &k, A, &m, tau, C, &m, wk, &nwk, &INF);
  return INF;
}
inline int left_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                      const float *A, float *tau, float *C, unsigned nwk,
                      float *wk) {
  int INF;
  Rsormqr("L", TRANS, &m, &n, &m, A, &m, tau, C, &m, wk, &nwk, &INF);
  return INF;
}
inline int left_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                      const unsigned k, const float *A, float *tau, float *C,
                      unsigned nwk, float *wk) {
  int INF;
  Rsormqr("L", TRANS, &m, &n, &k, A, &m, tau, C, &m, wk, &nwk, &INF);
  return INF;
}
// RIGHT: CQ(^T)
inline int right_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                       const double *A, double *tau, double *C, unsigned nwk,
                       double *wk) {
  int INF;
  Rdormqr("R", TRANS, &m, &n, &n, A, &n, tau, C, &m, wk, &nwk, &INF);
  return INF;
}
inline int right_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                       const unsigned k, const double *A, double *tau,
                       double *C, unsigned nwk, double *wk) {
  int INF;
  Rdormqr("R", TRANS, &m, &n, &k, A, &n, tau, C, &m, wk, &nwk, &INF);
  return INF;
}
inline int right_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                       const float *A, float *tau, float *C, unsigned nwk,
                       float *wk) {
  int INF;
  Rsormqr("R", TRANS, &m, &n, &n, A, &n, tau, C, &m, wk, &nwk, &INF);
  return INF;
}
inline int right_ormqr(const char *TRANS, const unsigned m, const unsigned n,
                       const unsigned k, const float *A, float *tau, float *C,
                       unsigned nwk, float *wk) {
  int INF;
  Rsormqr("R", TRANS, &m, &n, &k, A, &n, tau, C, &m, wk, &nwk, &INF);
  return INF;
}

// Cholesky decomposition: A=LL^T (if A is symmetric definite positive)
inline int potrf(const unsigned m, double *A, const unsigned n) {
  int INF;
  Rdpotrf("L", &m, A, &n, &INF);
  return INF;
}
inline int potrf(const unsigned m, float *A, const unsigned n) {
  int INF;
  Rspotrf("L", &m, A, &n, &INF);
  return INF;
}

} // end namespace Blas

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#endif // BLAS_HPP
