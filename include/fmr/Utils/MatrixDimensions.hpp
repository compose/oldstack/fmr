/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef MATRIXDIMENSIONS_HPP
#define MATRIXDIMENSIONS_HPP

// std includes
#include <limits>
#include <stdexcept>

// FMR includes
#include "fmr/Utils/Global.hpp"

namespace fmr {
// Check if size can be represented by int, otherwise crashes and return error
// message!
template <class Int_type> static void is_int(Int_type size) {
  if (!(size < (Int_type)std::numeric_limits<int>::max()))
    throw std::runtime_error("Size is too large to be converted to integer!");
}

// Partiton nbElements into nbPart
// Equally means the first partitions have the same size nbElement/nbPart and
// the final has the rest This not well balanced but this can be done afterwards
template <class Int_type>
static void partition_domain_equally(Int_type nbElement, int nbPart,
                                     Int_type *partitions) {
  Int_type nbValuesLeft = nbElement;
  for (int idxPart = 0; idxPart < nbPart - 1; ++idxPart) {
    partitions[idxPart] = Size(nbElement / nbPart);
    nbValuesLeft -= partitions[idxPart];
  }
  partitions[nbPart - 1] = nbValuesLeft;
}

} /* namespace fmr */

#endif // MATRIXDIMENSIONS_HPP
