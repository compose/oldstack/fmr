/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef MATRIXIOARRAY_HPP
#define MATRIXIOARRAY_HPP

// std includes
#include <gzstream.hpp>
#include <bzstream.hpp>
#include <numeric>
#include <stdexcept>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <typeinfo>
#include <memory> //< For std::unique_ptr, should we use this in place of (all?some?) ptrs?

// Utilities
#include "fmr/Utils/Global.hpp"
#include "fmr/Utils/Parameters.hpp"

namespace fmr {
/**
 * @brief The IO namespace
 * Beware! Only handles problem sizes representable by int32.
 */
namespace io {

  /* @brief default colnames when colnames is unused */
  static std::vector<std::string> dummy_colnames;
  /* @brief default rownames when rownames is unused */
  static std::vector<std::string> dummy_rownames;

  /**
   * @brief read a matrix from a binary file (nrows and ncols given as first elements to read)
   * @param[in] filename the file name where the matrix is store
   * @param[out] nrows the number of row of the matrix
   * @param[out] ncols the number of column of the matrix
   * @param[out] data a pointer on the array of elements (should be nullptr)
   */
  template<class SizeType, class ValueType>
  void readBinary( const std::string& filename, SizeType &nrows, SizeType &ncols, ValueType* &data, const int verbose = 0){

    // start reading process
    if (data != nullptr ) {
        delete [] data ;
      }
    std::ifstream stream(filename.c_str(),
                         std::ios::in | std::ios::binary | std::ios::ate);
    const std::ifstream::pos_type filesize = stream.tellg();
    if (filesize<=0) {
      std::cout << "Info: The requested binary file " << filename
                << " does not yet exist. Compute it now ... " << std::endl;
      return;
    }
    if (stream.good()) {
      stream.seekg(0);
      // 1) read size of problem (int)
      int nrow;
      stream.read(reinterpret_cast<char*>(&nrow), sizeof(int));
      nrows=nrow;
      // 2) read low rank (int)
      int ncol;
      stream.read(reinterpret_cast<char*>(&ncol), sizeof(int));
      ncols=ncol;
      if(verbose)
        std::cout << "About to read an " << nrows << " by " << ncols << " matrix..." << std::endl;
      // 1) read C
      data = new ValueType[nrows*ncols];
      stream.read(reinterpret_cast<char*>(data), sizeof(ValueType)*nrows*ncols);
    }
    else {
        throw std::runtime_error("File could not be opened to read");}
    stream.close();
  }

  template<class SizeType, class ValueType>
  void writeBinary(const SizeType nrows, const SizeType ncols, ValueType* data, const std::string& filename){

    // store into binary file
    std::ofstream stream(filename.c_str(),
                         std::ios::out | std::ios::binary | std::ios::trunc);
    if (stream.good()) {
      stream.seekp(0);
      // 1) write nrows of problem
      is_int(nrows);
      int _nrows = int(nrows);
      stream.write(reinterpret_cast<char*>(&_nrows), sizeof(int));
      // 2) write low rank (int)
      is_int(ncols);
      int _ncols = int(ncols);
      stream.write(reinterpret_cast<char*>(&_ncols), sizeof(int));
      // 1) write C
      stream.write(reinterpret_cast<char*>(data), sizeof(ValueType)*nrows*ncols);
    }
    else throw std::runtime_error("File could not be opened to write");
    stream.close();

  }

  /**
   * @ingroup InputOutput
   * @brief Read a dense matrix in a ASCII file
   *
   *  The ASCII file contains a dense matrix stored row by row.
   *  @param[in] nrows    number of line
   *  @param[in] ncols    number of column
   *  @param[out] data    the array of values read in file
   *  @param[in] filename full name of the matrix file
   *  @param[in] nskip    number of line to skip in the file (default: 0)
   *  @param[in] delim    the delimiter between two value (default: ' ')
   *  @param[in] bcns true to read colnames in the first line of the file
   *  (default: false)
   *  @param[in] brns true to read rownames in the first column of the file
   *  (default: false)
   *  @param[in] colmaj whether or not data is given in column major format
   * (default: true)
   *
   */
  template<class SizeType, class ValueType, class StreamType>
  static void readASCII(const SizeType& nrows, const SizeType& ncols, ValueType* &data,
                        const std::string& filename, const int nskip=0, const char delim=' ',
                        bool bcns=false, bool brns=false,  const bool colmaj=true)
  {
    StreamType file;
    std::string line;
    file.open(filename.c_str());
    // check if file exits
    if ( ! file.good()) {
        std::cerr << "[fmr] ERROR: Opening file `" << filename.c_str() << "' failed.\n";
        std::exit(EXIT_FAILURE);
    }
    // Skip the header
    for (int i = 0; i < nskip; ++i){
      std::getline(file, line);
    }
    // read the colnames and rownames if needed
    if ( bcns ){
        std::getline(file, line);
    }
    SizeType idx;
    for (SizeType i = 0; i < nrows; ++i) {
      std::getline(file, line);
      std::stringstream iss(line);
      if ( brns ){
          std::string val;
          std::getline(iss, val, delim);
      }
      for (SizeType j = 0; j < ncols; ++j) {
        idx = (colmaj) ? j*nrows+i : i*nrows+j;
        std::string val;
        std::string::size_type sz;
        std::getline(iss, val, delim);
        data[idx] = (ValueType)std::stof(val,&sz);
      }
    }
    file.close();
  }

  /**
   *
   * @ingroup InputOutput
   * @brief Read a dense matrix in a ASCII file
   *
   * The ASCII file contains a dense matrix stored row by row. The number of row and
   * column are unknown
   *
   * @param[out] nrows   number of line
   * @param[out] ncols   number of column
   * @param[out] data    the array of values read in file
   * @param[in] filename full name of the matrix file
   * @param[in] nskip    number of line to skip in the file (default: 0)
   * @param[in] delim    the delimiter between two value (default: ' ')
   * @param[in] bcns true to read colnames in the first line of the file (default:
   * false)
   * @param[inout] colnames give columns tags if bcns is true. If h5 fmt, colnames
   * gives the name of the dataset to read in input.
   * @param[in] brns true to read rownames in the first column of the file
   * (default: false)
   * @param[inout] rownames give rows tags if brns is true. If h5 fmt, rownames
   * gives the name of the dataset to read in input.
   * @param[in] colmaj whether or not data is given in column major format
   * (default: true)
   *
   */
  template<class SizeType, class ValueType, class StreamType>
  static void readASCII_ukSize(SizeType &nrows, SizeType &ncols, ValueType* &data,
                               const std::string& filename, const int nskip=0, const char delim=' ',
                               bool bcns=false, std::vector<std::string> &colnames=dummy_colnames,
                               bool brns=false, std::vector<std::string> &rownames=dummy_rownames,
                               const bool colmaj=true)
  {
    StreamType file;
    file.open(filename.c_str());
    // check if file exits
    if ( ! file.good()) {
        std::cerr << "[fmr] ERROR: Opening file `" << filename.c_str() << "' failed.\n";
        std::exit(EXIT_FAILURE);
    }
    // Init counter
    SizeType countRow = 0;
    SizeType countCol = 0;
    // Skip nskip lines if needed
    for (int i = 0; i < nskip; ++i){
      std::string line;
      std::getline(file, line);
    }
    // Find the number of Row and Column
    std::string line;
    // read the colnames and rownames if needed
    if ( bcns ){
        std::getline(file, line);
        std::stringstream iss(line);
        std::string val;
        while(std::getline(iss, val, delim)) {
            if (val.size() > 0){
                colnames.push_back(val);
                //std::cout << "val header " << val << std::endl;
            }
        }
    }
    while(std::getline(file, line)) {
      std::stringstream iss(line);
      SizeType countColTmp = 0;
      std::string val;
      //
      while(std::getline(iss, val, delim)) {
          if ( brns && countColTmp == 0 ){
              rownames.push_back(val);
              //std::cout << "val rowsname " << val << std::endl;
          }
          ++countColTmp;
      }
      if ( brns ) {
          --countColTmp;
      }
      if (countRow > 0){
        if( countCol != countColTmp) {
          std::cerr << "[fmr] read file: Wrong number of column in line " << countRow << std::endl
                    << "[fmr] read file: The column number is " << countColTmp << " rather than " << countCol  <<std::endl;
          std::exit(EXIT_FAILURE) ;
        }
      }
      countCol = std::max(countCol,countColTmp);
      ++countRow;
    }
    file.close();
    // Allocate the array
    data = new ValueType [countCol*countRow];
    // read the coefficients
    readASCII<SizeType, ValueType, StreamType>(countRow, countCol, data, filename, nskip, delim, bcns, brns, colmaj);
    std::cout << "[fmr] read file: countRow=" << countRow << "\n" ;
    std::cout << "[fmr] read file: countCol=" << countCol << "\n" ;
    // Update actual sizes
    nrows = countRow;
    ncols = countCol;
  }

  /**
   *
   * @brief Write data to a ASCII file
   *
   * @param[in] nrows number of line of data
   * @param[in] ncols number of column of data
   * @param[in] data data values to write
   * @param[in] filename full name of the data file
   * @param[in] nskip number of line to skip in the file (default: 0)
   * @param[in] delim the delimiter between two value (default ' ')
   * @param[in] ncolstosave number of columns to save of data matrix, may be lower
   * than the number of columns but not larger. If set to 0 ncolstosave is the
   * number of columns in data (default: 0).
   * @param[in] colmaj whether or not data is given in column major format
   * (default: true)
   * @param[in] bcns to save colnames in the first row of the file (default:
   * false)
   * @param[in] colnames the cols names (tags)
   * @param[in] brns true to save rownames in the first column of the file
   * (default: false)
   * @param[in] rownames the rows names (tags)
   * @param[in] data_fmt number of digits to save when writing ascii files
   * (default: 0 meaning save all meaningfull values)
   */
  template<class SizeType, class ValueType, class StreamType>
  static void writeASCII(const SizeType& nrows, const SizeType& ncols, ValueType *data,
                         const std::string& filename, const SizeType nskip=0, const char delim=' ',
                         const size_t ncolstosave=0, const bool colmaj=true,
                         bool bcns=false, std::vector<std::string> &colnames=dummy_colnames,
                         bool brns=false, std::vector<std::string> &rownames=dummy_rownames,
                         int data_fmt=0)
  {

      SizeType ncolsts = (ncolstosave==0) ? ncols : (SizeType)ncolstosave ;

      StreamType file( filename.c_str() );

      if ( data_fmt > 0) {
          file.precision(data_fmt);
      } else {
          file.precision(std::numeric_limits<ValueType>::digits10 + 2);
      }

      for (SizeType i = 0; i < nskip ; ++i) {
          file << std::endl;
      }

      if ( bcns ) {
          if ( brns ) {
              file << delim;
          }
          for (SizeType j = 0 ; j < ncolsts ; ++j) {
              file << colnames[j];
              if ( j < ncolsts-1 ) {
                  file << delim;
              }
          }
          file << std::endl;
      }
      for (SizeType i = 0; i < nrows ; ++i) {
          if ( brns ) {
              file << rownames[i] << delim;
          }
          for (SizeType j = 0 ; j < ncolsts ; ++j) {
              if ( colmaj ) {
                  file << data[i+j*nrows];
              } else {
                  file << data[j+i*ncols];
              }
              if ( j < ncolsts-1 ) {
                  file << delim;
              }
          }
          file << std::endl;
      }
      file.close();
  }

}} /* namespace fmr::io */

#endif // MatrixIOARRAY_HPP
