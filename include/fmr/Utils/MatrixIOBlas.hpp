/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#ifndef MATRIXIOBLAS_HPP
#define MATRIXIOBLAS_HPP

#include "fmr/Utils/MatrixIOArray.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_HDF5)
#include "hdf5.h"
#endif
#if defined(FMR_USE_MPI)
#include "mpi.h"
#endif

namespace fmr {

namespace io {

/**
 * @brief Read a dense matrix in a ASCII file
 *
 *  The ASCII file contains a dense matrix stored row by row.
 *  @param[out] data    the array of values read in file
 *  @param[in] filename full name of the matrix file
 *  @param[in] nskip    number of line to skip in the file (default: 0)
 *  @param[in] delim    the delimiter between two value (default: ' ')
 *  @param[in] bcns true to read colnames in the first line of the file
 *  (default: false)
 *  @param[in] brns true to read rownames in the first column of the file
 *  (default: false)
 *  @param[in] colmaj whether or not data is given in column major format
 * (default: true)
 *
 */
template<typename FSize, typename FReal, class StreamType, class DenseWrapper>
void readASCII(DenseWrapper &data,
               const std::string& filename, const int nskip=0, const char delim=' ',
               bool bcns=false, std::vector<std::string> &colnames=dummy_colnames,
               bool brns=false, std::vector<std::string> &rownames=dummy_rownames,
               const bool colmaj=true){

    FSize nrows;
    FSize ncols;
    FReal *matrixlapack = nullptr;

    readASCII_ukSize<FSize, FReal, StreamType>(
        nrows, ncols, matrixlapack,
        filename, nskip, delim, bcns, colnames, brns, rownames, colmaj);

    if (matrixlapack != nullptr ){
        /* fill wrapper with lapack matrix values */
        data.reset(nrows, ncols, matrixlapack);
        delete [] matrixlapack;
    } else {
        std::cerr << "[fmr] readASCII failed" << std::endl;
        std::exit(EXIT_FAILURE);
    }

}

/**
 *
 * @brief Write data to a ASCII file
 *
 * @param[in] data data values to write
 * @param[in] filename full name of the data file
 * @param[in] nskip number of line to skip in the file (default: 0)
 * @param[in] delim the delimiter between two value (default ' ')
 * @param[in] ncolstosave number of columns to save of data matrix, may be lower
 * than the number of columns but not larger. If set to 0 ncolstosave is the
 * number of columns in data (default: 0).
 * @param[in] colmaj whether or not data is given in column major format
 * (default: true)
 * @param[in] bcns to save colnames in the first row of the file (default:
 * false)
 * @param[in] colnames the cols names (tags)
 * @param[in] brns true to save rownames in the first column of the file
 * (default: false)
 * @param[in] rownames the rows names (tags)
 * @param[in] data_fmt number of digits to save when writing ascii files
 * (default: 0 meaning save all meaningfull values)
 */
template<class FSize, class FReal, class StreamType, class DenseWrapper>
void writeASCII(DenseWrapper &data,
                const std::string& filename, const int nskip=0, const char delim=' ',
                const size_t ncolstosave=0, const bool colmaj=true,
                bool bcns=false, std::vector<std::string> &colnames=dummy_colnames,
                bool brns=false, std::vector<std::string> &rownames=dummy_rownames,
                int data_fmt=0){

    FSize nrows = data.getNbRows();
    FSize ncols = data.getNbCols();
    FReal *lapackmatrix = data.getBlasMatrix();

    int mpiRank = 0;
#if defined(FMR_USE_MPI)
    int mpiinit;
    MPI_Initialized(&mpiinit);
    if (mpiinit) {
      MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    }
#endif

    if ( mpiRank == 0 ){
        writeASCII<FSize, FReal, StreamType>(
            nrows, ncols, lapackmatrix, filename, 0, delim,
            ncolstosave, colmaj, bcns, colnames, brns, rownames, data_fmt);
    }
}

#if defined(FMR_HDF5)

/**
 * @brief copy the element from one or several HDF5 files in the descriptor
 *
 * @param[in] data the dense wrapper matrix to fill
 * @param[in] filename    HDF5 filename on disk or metadata txt file.
 *                        If the global matrix is made of several hdf5 files the
 *                        metadata ASCII file should contain the following information:
 *                        Number of blocks e.g. 3
 *                        triangular part of the matrix given by the blocks e.g. upper
 *                        number line, number column of the block, file name, dataset name e.g.
 *                        0 0 atlas_guyane_trnH_0.h5  distance
 *                        0 1 atlas_guyane_trnH_1.h5  distance
 *                        0 2 atlas_guyane_trnH_2.h5  distance
 *                        1 1 atlas_guyane_trnH_3.h5  distance
 *                        1 2 atlas_guyane_trnH_4.h5  distance
 *                        2 2 atlas_guyane_trnH_5.h5  distance
 * @param[in] datasetname HDF5 dataset to consider (several datasets can be
 *            stored in one file) or root directory of the h5 files.
 */
template<class FSize, class FReal>
void readHDF5(BlasDenseWrapper<FSize, FReal> &data, const std::string& filename, const std::string& datasetname){

    if (filename.find(".h5") != std::string::npos) {

        // open the HDF5 file
        hid_t file = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        // open the specific dataset
        hid_t dataset = H5Dopen2(file, datasetname.c_str(), H5P_DEFAULT);
        // get dataspace of the dataset
        hid_t dspace = H5Dget_space(dataset);
        const int ndims = H5Sget_simple_extent_ndims(dspace);
        // the dimensions given in the constructor and the ones of the file
        // should match
        AssertLF(ndims == 2); // we consider matrices here
        hsize_t dims[2];
        H5Sget_simple_extent_dims(dspace, dims, NULL);
        H5Sclose(dspace);

        // set dimensions and allocate dense matrix
        FSize nrows = dims[0];
        FSize ncols = dims[1];
        data.reset(nrows, ncols);
        data.allocate();
        FReal* matrix = data.getMatrix();

        // copy from dataset into matrix
        const auto H5_TYPE = (sizeof(FReal) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE;
        H5Dread(dataset, H5_TYPE, H5S_ALL, H5S_ALL, H5P_DEFAULT, matrix);
        H5Dclose(dataset);
        H5Fclose(file);

        // hdf5 is row major while blasdensewrapper works in column major
        FReal *matrixt = new FReal[nrows * ncols];
        for (FSize i = 0; i < nrows; ++i) {
            for (FSize j = 0; j < ncols; ++j) {
                matrixt[i + j * nrows] = matrix[i*ncols + j];
            }
        }
        memcpy(matrix, matrixt, nrows * ncols * sizeof(FReal));
        delete [] matrixt;

    } else {
        std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
        std::cerr << "[fmr] filename is not a hdf5 file " << filename.c_str() << std::endl;
    }

}

template<class FSize, class FReal>
void writeHDF5(const BlasDenseWrapper<FSize, FReal>& data, const std::string& filename, const std::string& datasetname, size_t ncolstosave=0){

    if (filename.find(".h5") != std::string::npos) {

        FSize nrows = data.getNbRows();
        FSize ncols = data.getNbCols();
        FReal* matrix = data.getMatrix();

        /* open the HDF5 file */
        hid_t file_id;
        H5E_BEGIN_TRY
        /* if file already exists open it instead of creating it */
        file_id = H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
        H5E_END_TRY
        if ( file_id >= 0 ) {
            /* if dataset already exist delete it */
            htri_t datasetexist = -1;
            H5E_BEGIN_TRY
            datasetexist = H5Lexists(file_id, datasetname.c_str(), H5P_DEFAULT);
            H5E_END_TRY
            if ( datasetexist > 0 ) {
                H5Ldelete(file_id, datasetname.c_str(), H5P_DEFAULT);
            }
            H5Fclose(file_id);
        }
        if ( file_id < 0 ) {
            /* create a new file */
            file_id = H5Fcreate(filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        } else {
            /* open file */
            file_id = H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
        }

        ncols = (ncolstosave==0) ? ncols : (FSize)ncolstosave;
        hsize_t dims[2] = {static_cast<hsize_t>(nrows),
                        static_cast<hsize_t>(ncols)};
        hid_t dataspace_id = H5Screate_simple(2, dims, NULL);

        const auto H5_TYPE = (sizeof(FReal) == 8) ? H5T_IEEE_F64LE : H5T_IEEE_F32LE;
        hid_t dataset_id = H5Dcreate2(file_id, datasetname.c_str(), H5_TYPE, dataspace_id,
                                    H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

        /* Transpose the matrix */
        FReal *matrixt = new FReal[nrows * ncols];
        for (FSize i = 0; i < nrows; ++i) {
            for (FSize j = 0; j < ncols; ++j) {
                matrixt[j + i * ncols] = matrix[i + j * nrows];
            }
        }
        /* write the matrix */
        H5Dwrite(dataset_id, H5_TYPE, H5S_ALL, H5S_ALL, H5P_DEFAULT, matrixt);
        H5Sclose(dataspace_id);
        H5Dclose(dataset_id);
        H5Fclose(file_id);
        delete[] matrixt;
    } else {
        std::cerr << "[fmr] Error in file " << __FILE__ << " line " << __LINE__ << std::endl;
        std::cerr << "[fmr] filename is not a hdf5 file " << filename.c_str() << std::endl;
    }
}

#endif

}} /* namespace fmr::io */

#endif // MatrixIO_HPP
