﻿set(tools
    generateLowRankMatrix.cpp
 )
add_executable(generateLowRankMatrix generateLowRankMatrix.cpp)
target_link_libraries(generateLowRankMatrix PRIVATE fmr)
