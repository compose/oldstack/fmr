/* Copyright 2022 INRIA
 *
 * This file is part of the FMR software package for Fast Methods for
 * Randomized numerical linear algebra.
 *
 * This software is governed by the CeCILL-C license under French law
 * and abiding by the rules of distribution of free software. You can
 * use, modify and/or redistribute the software under the terms of the
 * CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 * URL: "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided
 * only with a limited warranty and the software's author, the holder of
 * the economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their
 * systems and/or data to be ensured and, more generally, to use and
 * operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-C license and that you accept its terms.
 */

#include <iomanip>
#include <iostream>
#include <limits>
#include <random>
#include <string>
#include <cstdlib>
#include <stdexcept>

#include "fmr/Algorithms/truncatedSVD.hpp"
#include "fmr/MatrixWrappers/BlasDenseWrapper.hpp"
#if defined(FMR_CHAMELEON)
#include "fmr/MatrixWrappers/ChameleonDenseWrapper.hpp"
#endif
#if defined(FMR_HDF5)
#include "hdf5.h"
#endif
#include "fmr/Algorithms/randomSVD.hpp"
#include "fmr/StandardLRA/Gemm.hpp"
#include "fmr/StandardLRA/QRF.hpp"
#include "fmr/StandardLRA/SVD.hpp"
#include "fmr/Utils/Display.hpp"
#include "fmr/Utils/MatrixIO.hpp"
#include "fmr/Utils/RandomMatrix.hpp"
#include "fmr/Utils/Tic.hpp"

#include <cpp_tools/cl_parser/cl_parser.hpp>
#include <cpp_tools/colors/colorized.hpp>

namespace localArgs {

struct rank { // No defaut type here (important to generate full matrix)
  cpp_tools::cl_parser::str_vec flags = {"--rank", "-r"};
  const char* description = " The rank to generate the matrix (int)";
  using type = int;
};

struct nbrows {
  cpp_tools::cl_parser::str_vec flags = {"--nrows", "-nr"};
  const char* description = "Number of rows (int)";
  using type = int;
  type def = 100;
};

struct nbcols {
  cpp_tools::cl_parser::str_vec flags = {"--ncols", "-nc"};
  const char* description = "Number of column (int)";
  using type = int;
  type def = 70;
};
struct useSeed {
  cpp_tools::cl_parser::str_vec flags = {"--seed", "-s"};
  const char* description = "The seed to generate the singular values  (int)";
  using type = int;
  type def = -1;
};
struct useMethod {
  cpp_tools::cl_parser::str_vec flags = {"--sv-decay", "-svd"};
  const char* description = "The singular values decay (between m = min(rank, min(ntows, ncols))\n"
      "1 uniform (between [0, 3*m] ) "
      "2 fast decay (d_j = 0.65^(j-1) *rand**2 ran uniform in [0,1]) \n"
      "3 slow decay (d_j = sqrt(1+200*(j-1))\n ";
  using type = int;
  type def = 1;
};
struct outputFile : cpp_tools::cl_parser::required_tag {
  cpp_tools::cl_parser::str_vec flags = {"--out-file", "-of"};
  const char* description =
      "Name of the ourtput file with extention (.txt, .bin, .h5)";
  using type = std::string;
  type def = "output.bin";
};

struct useFloat {
  cpp_tools::cl_parser::str_vec flags = {"--float", "-f"};
  std::string description =
      "To generate a matrix in float otherwise in double.";
  using type = bool;
  enum { flagged };
};
struct useBlas {
  cpp_tools::cl_parser::str_vec flags = {"--blas", "-b"};
  std::string description =
      "Do the test on BlasDenseWrapper instead of the default "
      "ChameleonDenseWrapper";
  using type = bool;
  enum { flagged };
};

struct useSym {
  cpp_tools::cl_parser::str_vec flags = {"--symmetric", "-symm"};
  std::string description = "To build a symmetric matrix";
  using type = bool;
  enum { flagged };
};
struct quiet {
  cpp_tools::cl_parser::str_vec  flags = {"--quiet", "-q"};
  std::string description = "No verbose node.";
  using type = bool;
  enum { flagged };
};

} // namespace localArgs

std::string method_str(const int method) {
  if (method == 1) {
    return std::string("exponential decays");
  } else if (method == 2) {
    return std::string("fast decays");
  } else if (method == 3) {
    return std::string("slow decays");
  }
  return std::string("none");
}
template <typename SEED_TYPE, typename VECTOR_TYPE>
void generate_singular_values(const int method, const SEED_TYPE& seed,
                              VECTOR_TYPE& singular_values) {
  using value_type = typename VECTOR_TYPE::value_type;
  auto rank = singular_values.size();
  /// Generate the reference singular values s;
  std::mt19937_64 generator(seed);

  // Switch to an uniform distribution
  if (method == 1) {
  } else if (method == 2) {
    // rank is arround 75
    std::uniform_real_distribution<value_type> distribution(0.0, 1.0);
    value_type beta{value_type(0.65)}, acc = 1.0;
    for (std::size_t i(0); i < rank; ++i) {
      auto g = distribution(generator);
      singular_values[i] = acc * g * g;
      acc *= beta;
    }
  } else if (method == 3) {
      for (std::size_t i(0); i < rank; ++i) {
        singular_values[i] = std::sqrt(1 + 200 * i);
      }
    }
  else {
    throw std::invalid_argument(
        "Method parameter for generate singular values should be 1,2 or 3");
  }
#ifdef GEOMETRIC_DECAY
  constexpr value_type sigma_min = std::numeric_limits<double>::epsilon();
  constexpr value_type sigma_max = value_type(100.);
  const auto nb_sv = std::min(p, n);
  const auto coeff = std::exp(std::log(sigma_min / sigma_max) / (nb_sv - 1));
  std::cout << "coeff: " << std::numeric_limits<double>::epsilon() << std::endl;
  ;
  singular_values[0] = sigma_max;
  for (int_type i(1); i < nb_sv; ++i) {
    singular_values[i] = coeff * singular_values[i - 1];
  }
#endif
}

template <class DenseWrapper, typename... Parameters >
auto generateLowRankMatrix(cpp_tools::cl_parser::parser<Parameters...> & parser) -> int {

  using value_type = typename DenseWrapper::value_type;
  using int_type = typename DenseWrapper::int_type;

  const bool verbose = !parser.template exists<localArgs::quiet>();

  const bool symm = parser.template exists<localArgs::useSym>();
  const int_type n{parser.template get<localArgs::nbrows>()};
  const int_type p{parser.template get<localArgs::nbcols>()};
  const int_type method{parser.template get<localArgs::useMethod>()};
  int_type rank{0};

  if (parser.template exists<localArgs::rank>() ) {
    rank = parser.template get<localArgs::rank>();
  } else {
    rank = std::min(n, p);
  }
  int mpiRank = 0;
#if defined(FMR_CHAMELEON)
  Chameleon::Chameleon chameleon(2, 0, 320);
  mpiRank = chameleon.getMpiRank();
#endif
  bool b_symm = n != p ? false : symm ;

  if (mpiRank == 0) {
    std::cout << cpp_tools::colors::blue  << "Generate lo rank matrix:\n";
    std::cout << "     Nrows           " << n << std::endl
              << "     Ncols           " << p << std::endl;
    std::cout << "     Symmetric       " <<std::boolalpha<< b_symm << std::endl;
    std::cout << "     Rank            " << rank << std::endl;
    std::cout << "     Singular Values " << method_str(method) << cpp_tools::colors::reset<< std::endl;
  }
  if (rank > std::min(p, n)) {
    throw std::invalid_argument("Error: rank > std::min(p, n) but should be lower\n");
  }
  DenseWrapper A(n, p);
  /////////////////////////////////////////////////////////////////////////
  /// Generate the matrix A with given rank and given singular values
  fmr::tools::RandomClass<value_type> randGen;
  int seed{parser.template get<localArgs::useSeed>()};
  if (seed == -1) {
    seed = std::random_device{}();
  }
  std::vector<value_type> singular_value_ref(rank, 0.0);
  generate_singular_values(method, seed, singular_value_ref);

    std::sort(singular_value_ref.begin(), singular_value_ref.end(),
              std::greater<value_type>());

    randGen.buildMatrixWithRank(singular_value_ref, A, b_symm, verbose);

  ///
  /////////////////////////////////////////////////////////////////////////
  ///
  fmr::Display::vector(singular_value_ref, "Singular values ", singular_value_ref.size());

  ///
  /// Save the matrix
  ///
  const std::string output_filename(parser.template get<localArgs::outputFile>());

  std::cout << "Write matrix in " << output_filename << std::endl;
  if (output_filename.find(".h5") != std::string::npos) {
#if defined(FMR_HDF5)
    fmr::io::writeHDF5(A, output_filename, "matrix");
#else
    std::cout << "H5 interface not activate. Compile with FMR_USE_HDF5\n";
#endif
  } else if (output_filename.find(".bin") != std::string::npos) {
    fmr::io::writeBinary(n, p, A.getBlasMatrix(), output_filename);
    //
    // int nn, pp;
    //   MatrixIO::read_bin(output_filename, nn, pp, A.getMatrix());
  }

  return EXIT_SUCCESS;
}

////
/// \brief main
/// \param argc
/// \param argv
/// \return
///  // Generate a matrix of size 30x15 (coefficient in double) with rank 4 in binary file matrix_30x15-8.bin
///  \code
///  tools/generateLowRankMatrix   -nrows 30 -ncols 15  --rank 8  --out-file matrix_30x15-8.bin
///  \endcode
///  Generate a symmetric matrix of size 30x130 with rank 10 in H5 file output.h5
///  \code
///   tools/generateLowRankMatrix  --nbRows 30 -nbCols 30 --symmetric --rank 10 --out-file output.h5
/// \endcode
/// Another example ith more parameters
/// tools/generateLowRankMatrix  --nrows 60 --ncols 40 --rank 20 --float --out-file matrix_60x40-20.bin
///           --sv-decay 1  -q --seed 0
/// \endcode
///
int main(int argc, char *argv[]) {

  // Parameter handling
  auto parser =
      cpp_tools::cl_parser::make_parser(cpp_tools::cl_parser::help{},
                                        localArgs::nbrows{},
                                        localArgs::nbcols{},
                                        localArgs::rank{},
                                        localArgs::useSeed{},
                                        localArgs::useSym{},
                                        localArgs::quiet{},
                                        localArgs::useBlas{},
                                        localArgs::useFloat{},
                                        localArgs::outputFile{},
                                        localArgs::useMethod{}
                                        );
  parser.parse(argc, argv);
  // Getting command line parameters


  using int_type = int;

#ifdef FMR_CHAMELEON
  if ( parser.exists<localArgs::useBlas>()) {

    if ( parser.exists<localArgs::useFloat>()) {
      generateLowRankMatrix<fmr::BlasDenseWrapper<int_type, float>>(parser);
    } else {
      generateLowRankMatrix<fmr::BlasDenseWrapper<int_type, double>>(parser);
    }
  }
  else {
    if (parser.exists<localArgs::useFloat>()) {
      generateLowRankMatrix<fmr::ChameleonDenseWrapper<int_type, float>>(parser);
    } else {
      generateLowRankMatrix<fmr::ChameleonDenseWrapper<int_type, double>>(parser);
    }
  }
#else
  if (parser.exists<localArgs::useFloat>()) {
    generateLowRankMatrix<fmr::BlasDenseWrapper<int_type, float>>(parser);
  } else {
    generateLowRankMatrix<fmr::BlasDenseWrapper<int_type, double>>(parser);
  }
#endif

  return EXIT_SUCCESS;
}
